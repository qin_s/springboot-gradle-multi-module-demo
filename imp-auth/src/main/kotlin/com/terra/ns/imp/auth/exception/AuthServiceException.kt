package com.terra.ns.imp.auth.exception

import com.terra.ns.imp.auth.constant.AuthModule
import com.terra.ns.imp.common.core.exception.ErrorCode
import com.terra.ns.imp.common.core.exception.ServiceException

/**
@author qins
@date 2023/5/25
@desc 用户授权模块异常
 */
class AuthServiceException(errorCode: ErrorCode, vararg params: String) : ServiceException(AuthModule.instance, errorCode, *params)