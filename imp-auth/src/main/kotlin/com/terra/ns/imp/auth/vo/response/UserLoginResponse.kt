package com.terra.ns.imp.auth.vo.response

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/6/8
@desc 用户登录响应
 */
@Schema(description = "用户登录响应")
data class UserLoginResponse(

    @Schema(description = "登录token")
    var token: String = "",
)