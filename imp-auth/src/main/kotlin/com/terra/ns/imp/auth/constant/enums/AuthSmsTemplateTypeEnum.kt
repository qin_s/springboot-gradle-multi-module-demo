package com.terra.ns.imp.auth.constant.enums

import com.terra.ns.imp.common.sms.SmsTemplateTypeDefinition

/**
@author qins
@date 2023/6/7
@desc 登录授权模块所用短信模板
 */
enum class AuthSmsTemplateTypeEnum(val type: Int, val desc: String) : SmsTemplateTypeDefinition {

    LOGIN(1, "登录"),

}