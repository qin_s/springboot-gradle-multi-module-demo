package com.terra.ns.imp.auth.vo.request

import com.terra.ns.imp.common.sms.vo.VerifyCaptchaEnableNullRequest
import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank

/**
@author qins
@date 2023/6/7
@desc
 */
@Schema(description = "用户登录请求实体")
data class LoginRequest(

    @Schema(description = "用户手机号", required = true)
    @get:NotBlank(message = "手机号不能为空")
    var phone: String = "",

    @Schema(description = "登录密码， 如果需要，跟验证码必须二选一")
    var password: String = "",

): VerifyCaptchaEnableNullRequest()