package com.terra.ns.imp.auth.constant

import com.terra.ns.imp.common.core.definition.AbstractModuleDefinition

class AuthModule private constructor(code: String = AuthConstants.MODULE_CODE,
                                     name: String = "登录授权",
                                     desc: String = "登录授权"): AbstractModuleDefinition(code, name, desc) {
    companion object {
         val instance = SingleHolder.instance
    }
    private object SingleHolder {
        val instance = AuthModule()
    }
}
