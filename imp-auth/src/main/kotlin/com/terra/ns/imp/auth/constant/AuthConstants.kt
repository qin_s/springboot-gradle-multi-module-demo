package com.terra.ns.imp.auth.constant

import com.terra.ns.imp.common.core.definition.ConstantDefinition

/**
@author qins
@date 2023/6/5
@desc  授权模块常量定义
 */
object AuthConstants: ConstantDefinition {

    const val MODULE_CODE = "imp-AUTH" // 模块编码
}