package com.terra.ns.imp.auth.service

import com.terra.ns.imp.auth.vo.request.LoginRequest
import com.terra.ns.imp.auth.vo.response.UserLoginResponse
import com.terra.ns.imp.system.api.vo.request.SmsCaptchaRequest

/**
@author qins
@date 2023/6/7
@desc 登录操作
 */
interface ILoginService {

    /**
     * 登录
     */
    fun login(request: LoginRequest): UserLoginResponse


    /**
     * 只凭手机号免密登录
     */
    fun passwordlessLogin(phone: String) : UserLoginResponse

    /**
     * 获取登录验证码
     */
    fun getLoginSmsCode(request: SmsCaptchaRequest): String


}