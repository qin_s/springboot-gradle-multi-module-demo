package com.terra.ns.imp.auth.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

/**
@author qins
@date 2023/6/7
@desc 用户登录属性配置
 */
@Configuration
@ConfigurationProperties(prefix = "user.login")
class UserLoginProperties {

    /**
     * 密码最大可错误次数
     */
    var maxRetryCount: Int = 3

    /**
     * 超过错误次数锁定时长，单位秒
     */
    var lockTime: Long = 300L

    /**
     * 三方登录成功重定向页面
     */
    var loginSuccessPage: String = ""

    /**
     * 三方登录未注册重定向页面
     */
    var noRegisterPage: String = ""
}