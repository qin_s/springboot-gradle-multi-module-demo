package com.terra.ns.imp.auth.controller

import com.terra.ns.imp.auth.constant.AuthConstants.MODULE_CODE
import com.terra.ns.imp.auth.service.LoginServiceImpl
import com.terra.ns.imp.auth.vo.request.LoginRequest
import com.terra.ns.imp.auth.vo.response.UserLoginResponse
import com.terra.ns.imp.common.log.OperateLog
import com.terra.ns.imp.common.log.OperateTypeEnum
import com.terra.ns.imp.common.satoken.utils.LoginUtils
import com.terra.ns.imp.common.vo.response.ResultBean
import com.terra.ns.imp.common.web.idempotent.Idempotent
import com.terra.ns.imp.system.api.vo.request.SmsCaptchaRequest
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.validation.Valid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
@author qins
@date 2023/5/25
@desc 登录授权
 */
@RestController
@RequestMapping("/")
@Tag(name = "login" )
class LoginAuthController {

    @Autowired
    private lateinit var loginAuthService: LoginServiceImpl

    @PostMapping("/login")
    @Operation(summary = "验证码或密码登录")
    @OperateLog(MODULE_CODE, OperateTypeEnum.LOGIN)
    @Idempotent
    fun login(@RequestBody @Valid request: LoginRequest): ResultBean<UserLoginResponse> {
        return ResultBean.success(loginAuthService.login(request))
    }

    @PostMapping("/getLoginSmsCode")
    @Operation(summary = "获取登录验证码", description = "返回验证码UUID")
    @OperateLog(MODULE_CODE, OperateTypeEnum.OTHER, "获取登录验证码")
    @Idempotent
    fun getLoginSmsCode(@RequestBody @Valid request: SmsCaptchaRequest): ResultBean<String> {
        return ResultBean.success(loginAuthService.getLoginSmsCode(request))
    }


    @PostMapping("/logout")
    @Operation(summary = "退出登录")
    @OperateLog(MODULE_CODE, OperateTypeEnum.LOGOUT)
    @Idempotent
    fun logout(): ResultBean<Any> {
        LoginUtils.logout()
        return ResultBean.success()
    }
}