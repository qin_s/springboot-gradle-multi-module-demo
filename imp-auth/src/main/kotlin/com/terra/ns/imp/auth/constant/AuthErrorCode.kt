package com.terra.ns.imp.auth.constant

import com.terra.ns.imp.common.core.definition.ErrorCodeDefinition
import com.terra.ns.imp.common.core.exception.ErrorCode

/**
@author qins
@date 2023/5/25
@desc 授权模块错误码 1-10-xxxxx
 */
object AuthErrorCode : ErrorCodeDefinition {

    val USER_NOT_EXIST = ErrorCode(11000001, "未找到用户信息")
    val LOGIN_VERIFY_PARAM_ERROR = ErrorCode(11000002, "请使用密码或短信验证码登录")
    val LOGIN_PASSWORD_ERROR = ErrorCode(11000003, "手机号或密码错误")
    val LOGIN_SMS_CAPTCHA_ERROR = ErrorCode(11000004, "验证码错误或已过期")

    val USER_NO_COMPANY = ErrorCode(11010000, "用户未归属到任何组织中")
    val USER_ACCOUNT_LOCKED = ErrorCode(11010001, "账号已被锁定，请稍后再试或联系管理员")
    val USER_ACCOUNT_DISABLE = ErrorCode(11010002, "账号已停用，请联系管理员")
    val USER_ACCOUNT_EXCEPTION = ErrorCode(11010003, "账号异常，请联系管理员")
    val USER_ACCOUNT_LOGOUT = ErrorCode(11010004, "账号已被注销")
    val USER_LOGIN_PASSWORD_ERROR = ErrorCode(11010005, "登录密码错误，连续错误超过{}次后账号将被锁定{}分钟")
    val NOT_TENANT_USER = ErrorCode(11010006, "用户没有不是组织成员，请联系管理员")

    val DINGDING_LOGIN_ERROR = ErrorCode(11020001, "钉钉授权登录失败")

}