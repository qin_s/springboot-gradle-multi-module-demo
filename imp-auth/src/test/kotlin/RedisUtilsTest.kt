import com.terra.ns.imp.auth.LpAuthApplication
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

/**
@author qins
@date 2023/6/1
@desc redis utils工具类测试用例
 */
@RunWith(SpringRunner::class)
@SpringBootTest(classes = [LpAuthApplication::class])
class RedisUtilsTest {

    @Test
    fun testGetKeyNull() {
//        println( RedisUtils.get<String>("TESTKEY_NULL_string") )
//            val value = RedisUtils.get<String>("TESTKEY_NULL_string1")
//            println( value?.substring(1) )
//        println( RedisUtils.get("TESTKEY_NULL_string1") as String )
//        println( RedisUtils.expire("TESTKEY_NULL_string1", Duration.ofSeconds(2)))
//        println( RedisUtils.delete("TESTKEY_NULL_string1"))
//        println( RedisUtils.deleteKeys("TESTKEY_NULL_string1*"))
//        println( RedisUtils.deleteKeys("TESTKEY_NULL_string1", "TESTKEY_NULL_string2"))
//        println( RedisUtils.getMap<String>("TESTKEY_NULL_string1"))
//        println( RedisUtils.getSet<String>("TESTKEY_NULL_string1"))
//        println( RedisUtils.getTimeToLive<String>("TESTKEY_NULL_string1"))
//        RedisUtils.set("baseUser", LoginUser("uc11","199999"))
//        val session = RedisUtils.get<SaSession>("lowcodeplatform:token:login:token-session:39qDdKvSHdlOaulpF57Y4xOh7Dl1kE1j")
//        val session = RedisUtils.get<SaSession>("lowcodeplatform:token:login:session:UC0001")
//        val session = RedisUtils.get<LoginUser>("baseUser")
//        println()
//        RedisUtils.setSet("test", listOf(1,2,3,4,5,6,5,5), Duration.ofMillis(20000))
//        val deptCodes = RedisUtils.getSet<Int>("test")
//        println("===============" + deptCodes.size)

    }
}