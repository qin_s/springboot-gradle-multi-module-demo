
dependencies {
    implementation(project(":imp-common:imp-common-web"))
    implementation(project(":imp-common:imp-common-log"))
    implementation(project(":imp-common:imp-common-sms"))
    implementation(project(":imp-api:imp-system-api"))
    implementation(project(":imp-common:imp-common-dingtalk"))
}