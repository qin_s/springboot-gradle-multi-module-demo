import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.io.FileInputStream
import java.util.*

plugins {
    alias(projectLibs.plugins.springboot)
    alias(projectLibs.plugins.kotlin.jvm)
    alias(projectLibs.plugins.kotlin.spring)
    alias(projectLibs.plugins.docker)
}
// 定义库引用
val pLibs = projectLibs
// 加载根路径自定义配置属性
val envProps = Properties()
envProps.load(FileInputStream("${project.projectDir.absolutePath}${File.separator}env.properties"))
subprojects {
    // 全部项目有基础插件kotlin jvm/spring
    apply {
        plugin(pLibs.plugins.kotlin.jvm.get().pluginId)
        plugin(pLibs.plugins.kotlin.spring.get().pluginId)
    }
    // 统一group和version
    group = "com.terra.ns.imp"
    version = pLibs.versions.imp.service.get()
    java.sourceCompatibility = JavaVersion.VERSION_17

    dependencies {
        // bom 管理着一组依赖的版本，各模块按需引入其中的依赖即可，由bom统一约束着版本，
        // spring boot bom
        implementation(platform(pLibs.spring.boot))
    }
    configurations.all {
        exclude("org.springframework.boot","spring-boot-starter-logging")
        exclude("ch.qos.logback")
    }
    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict", "-Xjvm-default=all")
            jvmTarget = "17"
        }
    }
    tasks.withType<Test> {
        useJUnitPlatform()
    }
    tasks.jar {
        enabled = true
        archiveClassifier.set("")
    }

    // 为拥有bootJar的模块创建dev,test,prod的构建任务
    if (getTasksByName("bootJar", false).isNotEmpty()) {
        println("getTask:::" + getTasksByName("bootJar", false))
        tasks.register<Jar>("bootJar-dev") { buildJarTaskConf(this, "dev") }
        tasks.register<Jar>("bootJar-test") { buildJarTaskConf(this, "test") }
        tasks.register<Jar>("bootJar-prod") { buildJarTaskConf(this, "prod") }

        // docker镜像
        apply {
            plugin(pLibs.plugins.docker.get().pluginId)
        }
        docker {
            // docker镜像仓库配置
            registryCredentials {
                url.set("http://10.0.12.191:8888")
                username.set("admin")
                password.set("admin123")
            }
            springBootApplication {
                baseImage.set("10.0.12.191:8888/common/java:17-latest")
                images.set(setOf("10.0.12.191:8888/lowcode/$name:$version"))
                jvmArgs.set(listOf("-Dfile.encoding=utf-8"))
            }
        }
        // docker测试环境构建并推送镜像任务
        tasks.register<Jar>("dockerBuildAndPushImage-test") { buildDockerTaskConf(this, "test") }
    }

    if (getTasksByName("bootJar", false).isNotEmpty() || project.name == "imp-common-web") {
        // 在复制文件任务processResources阶段替换变量
        tasks.processResources {
            doFirst {  // 将逻辑放到执行阶段
                val env = System.getProperty("buildEnv", "dev") // 默认dev环境
                println("===============模块 ${project.name} 替换配置文件环境变量 , 构建环境: $env ==================")
//                filter(
//                    ReplaceTokens::class,
//                    "tokens" to
////							envPropsToMap(env)
//                            // todo 写个函数自动读取待替换列表转为map
//                            mapOf(
//                                "nacos.server" to envProps.getOrElse("env.$env.nacos.server") {
//                                    throw IllegalArgumentException(
//                                        "未找到环境：$env 属性：nacos.server 的配置"
//                                    )
//                                },
//                                "nacos.namespace" to envProps.getOrElse("env.$env.nacos.namespace") {
//                                    throw IllegalArgumentException(
//                                        "未找到环境：$env 属性：nacos.namespace 的配置"
//                                    )
//                                },
//                                "nacos.username" to envProps.getOrDefault("env.$env.nacos.username", ""),
//                                "nacos.password" to envProps.getOrDefault("env.$env.nacos.password", ""),
//                                "active.env" to env
//                            )
//                )
                filteringCharset = "UTF-8"
            }
        }
        tasks.classes {
            dependsOn("clean")
        }
    }
}
// docker服务地址
docker {
    url.set("tcp://10.0.12.191:2375")
}

/**
 * 任务执行顺序说明
 * Task#finalizedBy 函数 的作用是为 Gradle 任务 设置任务执行完毕后执行的任务，A.finalizedBy B 的作用是 A 任务执行完毕后 , 执行 B 任务
 * A.finalizedBy B
 * B.dependsOn C
 * A 执行完毕后执行 B , B 依赖于 C , 执行 B 之前要先把 C 执行了。A -> C -> B
 */
/**
 * 构建各个环境的bootJar
 */
fun buildJarTaskConf(jarTask: Jar, env: String) {
    jarTask.group = "build"
    jarTask.dependsOn("clean")
    jarTask.doFirst {
        println("=============== 构建 ${jarTask.archiveFileName.get()} 包 , 构建环境: $env ==================")
        System.setProperty("buildEnv", env)
    }
    jarTask.finalizedBy("bootJar")
}

fun buildDockerTaskConf(dockerPushImage: Jar, env: String) {
    dockerPushImage.group = "docker"
    dockerPushImage.doFirst {
        System.setProperty("buildEnv", env)
    }
    dockerPushImage.finalizedBy("dockerPushImage")
}
