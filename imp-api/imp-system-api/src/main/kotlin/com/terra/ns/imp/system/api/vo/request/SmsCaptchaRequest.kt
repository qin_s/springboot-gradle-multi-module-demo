package com.terra.ns.imp.system.api.vo.request

import com.terra.ns.imp.common.vo.entity.BaseEntity
import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull

/**
@author qins
@date 2023/6/7
@desc 获取短信验证码请求
 */
@Schema(description = "获取短信验证码请求")
data class SmsCaptchaRequest(

    @Schema(description = "手机号", required = true)
    @get:NotBlank(message = "手机号不能为空")
    var phone: String = "",

    @Schema(description = "模板编码， 默认验证码模板", hidden = true)
    @get:NotNull(message = "短信模板类型不能为空")
    var templateType: Int = -1
) : BaseEntity()