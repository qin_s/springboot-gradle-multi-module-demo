package com.terra.ns.imp.system.api.entity

import com.terra.ns.imp.common.vo.entity.BaseEntity

class BaseDept(var name: String, var code: String) : BaseEntity() {
    // 空构造函数，序列化用
    constructor() : this("", "")
}