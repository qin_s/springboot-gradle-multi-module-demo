package com.terra.ns.imp.system.api.entity

import com.baomidou.mybatisplus.annotation.TableName
import com.terra.ns.imp.common.vo.entity.BaseTableField
import java.time.LocalDateTime

/**
 * <p>
 * 系统操作日志记录表
 * </p>
 * @author qins
 * @since 2023/06/02
 */
@TableName("st_operate_log")
class StOperateLog : BaseTableField() {

    var traceId: String? = null
    /**
     * 操作账号
     */
    var operatorAccount: String? = null
    /**
     * 操作者名称
     */
    var operatorName: String? = null
    /**
     * 操作者IP
     */
    var operatorIp: String? = null
    /**
     * 模块名称
     */
    var moduleName: String? = null
    /**
     * 请求地址
     */
    var requestUrl: String? = null

    /**
     * 方法名称
     */
    var requestMethod: String? = null
    /**
     * 请求参数
     */
    var requestParam: String? = null
    /**
     * 操作类型枚举：add，delete，modify，query，unknown
     */
    var operateType: String? = null

    /**
     * 响应数据
     */
    var responseData: String? = null

    /**
     * 操作结果
     */
    var operateResult: String? = null

    /**
     * 操作时间
     */
    var operateTime: LocalDateTime? = null

    /**
     * 提示信息
     */
    var resultMessage: String? = null

    /**
     * 处理耗时
     */
    var handleTime: Long? = null


}
