package com.terra.ns.imp.system.api.service

import com.terra.ns.imp.system.api.entity.StOperateLog

/**
@author qins
@date 2023/6/2
@desc 操作日志API接口
 */
interface IOperateLogApi {


    /**
     * 保存操作日志
     * @return 保存结果
     */
    fun saveOperateLog(log: StOperateLog) : Boolean
}