package com.terra.ns.imp.system.api.entity

import com.terra.ns.imp.common.vo.entity.BaseEntity

/**
@author qins
@date 2023/7/12
@desc 用户部门基础信息
 */
class UserDeptInfo(var userCode: String, var phone: String, var userName: String, var deptCode: String?, var deptName: String?) : BaseEntity() {
    // 空构造函数，序列化用
    constructor() : this("", "", "", null, null)
}
