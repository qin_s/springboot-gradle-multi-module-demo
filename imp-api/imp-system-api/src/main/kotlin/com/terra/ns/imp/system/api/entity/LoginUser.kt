package com.terra.ns.imp.system.api.entity

/**
@author qins
@date 2023/6/8
@desc
 */
class LoginUser(var code: String, var phone: String,  var name: String) {

    // 空构造函数，序列化用
    constructor() : this("", "", "")
}
