package com.terra.ns.imp.system.api.service

import com.terra.ns.imp.system.api.entity.BaseDeptUser

/**
@author qins
@date 2023/7/10
@desc 部门API接口
 */
interface IDeptApi {

    /**
     * 根据多个部门编码获全部用户
     * @param deptCodes 部门编码列表
     */
    fun getAllUserByDepts(deptCodes: List<String>) : List<BaseDeptUser>

    /**
    * 获取指定部门主管
    * @param deptCodes 指定部门编码列表
    */
    fun getDeptLeaderByDepts(deptCodes: List<String>) : List<BaseDeptUser>
}