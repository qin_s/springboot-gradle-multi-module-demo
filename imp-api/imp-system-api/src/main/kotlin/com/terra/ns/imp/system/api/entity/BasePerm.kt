package com.terra.ns.imp.system.api.entity

import com.terra.ns.imp.common.vo.entity.BaseEntity

/**
@author qins
@date 2023/6/8
@desc 基础权限信息
 */
class BasePerm(var name: String, var code: String) : BaseEntity() {
    // 空构造函数，序列化用
    constructor() : this("", "")
}