package com.terra.ns.imp.system.api.service

import com.terra.ns.imp.system.api.vo.request.SmsCaptchaRequest

/**
@author qins
@date 2023/6/7
@desc 短信验证码API接口
 */
interface ISmsCaptchaApi {

    /**
     * 获取短信验证码
     */
    fun getPhoneCaptcha(request: SmsCaptchaRequest): String

    /**
     * 校验短信验证码
     * @param uuid 验证码唯一标识
     * @param phone 手机号
     * @param code 验证码
     * @param type 短信验证码场景类型
     */
    fun verifySmsCaptcha(uuid: String?, phone: String?, type: Int?, code: String?) : Boolean
}