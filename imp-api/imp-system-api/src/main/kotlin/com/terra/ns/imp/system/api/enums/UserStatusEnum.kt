package com.terra.ns.imp.system.api.enums

/**
@author qins
@date 2023/6/7
@desc
 */
enum class UserStatusEnum(val code: Int, val desc: String) {

    NORMAL(0, "正常"),
    LOCKED(1, "锁定"),
    EXCEPTION(2, "异常"),
    DISABLE(3, "停用"),
    LOGOUT(-999, "注销");

    companion object {
        // 获取未注销的状态
        fun getNotLogoutStatus() : List<Int>
                = values().filter { it.code != LOGOUT.code }.map { it.code }

        // 获取未注销的状态
        fun getDesc(status: Int?) : String?
                = values().firstOrNull { it.code == status }?.desc
    }
}
