package com.terra.ns.imp.system.api.service

import com.terra.ns.imp.system.api.entity.*

/**
@author qins
@date 2023/6/1
@desc 用户信息API接口
 */
interface IUserApi {

    /**
     * 根据手机号获取用户信息
     * @param phone 手机号
     * @return DB基础用户
     */
    fun getUserByPhone(phone: String) : StUser?

    /**
     * 获取用户拥有的角色
     * @param userCode 用户编码
     * @param tenantId 租户ID
     * @return 角色基础信息列表
     */
    fun getUserRoles(userCode: String) : List<BaseRole>

    /**
     * 获取用户拥有的权限
     * @param userCode 用户编码
     * @param tenantId 租户ID
     * @return 用户权限基础信息列表
     */
    fun getUserPerms(userCode: String): List<BasePerm>

    /**
     * 获取用户所在部门
     * @param userCode 用户编码
     * @param tenantId 租户ID
     */
    fun getUserDepts(userCode: String): List<BaseDept>

    /**
     * 获取用户所在部门的主管
     * @param userCode 用户编码
     * @param tenantId 租户ID
     */
    fun getUserDeptLeader(userCode: String): List<BaseDeptUser>

    /**
     * 获取用户部门信息
     * @param userCodes 用户编码集合
     * @param tenantId 租户ID
     */
    fun getUserDeptInfo(userCodes: List<String>) : List<UserDeptInfo>

    /**
     * 获取用户部门信息
     * @param userCode 用户编码
     * @param tenantId 租户ID
     */
    fun getUserDeptInfo(userCode: String) : UserDeptInfo?
}