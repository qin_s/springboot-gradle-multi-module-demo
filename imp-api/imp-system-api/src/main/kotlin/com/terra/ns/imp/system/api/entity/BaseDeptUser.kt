package com.terra.ns.imp.system.api.entity

import com.terra.ns.imp.common.vo.entity.BaseEntity

/**
@author qins
@date 2023/7/10
@desc 基础部门-用户关系
 */
class BaseDeptUser(var deptCode: String, var userCode: String): BaseEntity() {
    // 空构造函数，序列化用
    constructor() : this("", "")
}