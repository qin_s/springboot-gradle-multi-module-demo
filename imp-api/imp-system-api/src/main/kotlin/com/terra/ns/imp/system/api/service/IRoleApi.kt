package com.terra.ns.imp.system.api.service

import com.terra.ns.imp.system.api.entity.BaseRoleUser

/**
@author qins
@date 2023/7/10
@desc 角色API接口
 */
interface IRoleApi {

    /**
     * 根据多个角色编码获全部用户
     * @param roleCodes 角色编码列表
     */
    fun getAllUserByRoles(roleCodes: List<String>) : List<BaseRoleUser>

}