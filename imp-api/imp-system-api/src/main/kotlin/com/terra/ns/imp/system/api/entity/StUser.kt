package com.terra.ns.imp.system.api.entity

import com.baomidou.mybatisplus.annotation.TableName
import com.terra.ns.imp.common.vo.entity.BaseTableOperatorField
import java.time.LocalDateTime

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
@TableName("st_user")
class StUser : BaseTableOperatorField() {


    /**
     * 用户唯一编码
     */
    var code: String? = null

    /**
     * 手机号-区别唯一用户
     */
    var phone: String? = null

    /**
     *  用户名称
     */
    var name: String? = null

    /**
     * 登录密码
     */
    var password: String? = null

    /**
     * 头像
     */
    var avatar: String? = null

    /**
     * 员工工号
     */
    var jobNumber: String? = null

    /**
     * 员工邮箱
     */
    var email: String? = null

    /**
     * 员工的企业邮箱
     */
    var orgEmail: String? = null

    /**
     * 入职时间
     */
    var hiredDate: LocalDateTime? = null

    /**
     * 职位
     */
    var position: String? = null

    /**
     * 绑定微信的unionId
     */
    var wxUnionId: String? = null

    /**
     * 账号全局状态：0：正常状态，1：锁定，2：异常 ，3：停用  -999：注销
     */
    var status: Int? = null
}
