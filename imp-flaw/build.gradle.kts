dependencies {
    implementation(project(":imp-common:imp-common-web"))
    implementation(project(":imp-common:imp-common-log"))
    implementation(project(":imp-common:imp-common-mybatis"))
    implementation(projectLibs.bundles.datasource)
    implementation(projectLibs.hutool.crypto)
}