import java.net.URI

dependencyResolutionManagement {
    repositories {
        // 使用阿里云 镜像仓库
        maven {url = URI("https://maven.aliyun.com/nexus/content/groups/public/") }
        //中黔私库
        maven { url = URI("https://code.zqfae.com/zqfae-repo/repository/zqfae-repo/") }
        mavenCentral()
    }
    // 统一依赖库定义
    versionCatalogs {
        create("projectLibs"){
            from(files("./libs.versions.toml"))
        }
    }
}
rootProject.name = "imp-service"
include("imp-common:imp-common-core")
include("imp-common:imp-common-log")
include("imp-common:imp-common-vo")
include("imp-common:imp-common-util")
include("imp-common:imp-common-web")
include("imp-common:imp-common-redis")
include("imp-common:imp-common-mybatis")
include("imp-common:imp-common-satoken")
include("imp-common:imp-common-log")
include("imp-common:imp-common-sms")
include("imp-common:imp-common-oss")
include("imp-common:imp-common-dingtalk")
include("imp-common:imp-common-dingtalk-contact")
include("imp-api")
include("imp-api:imp-system-api")
include("imp-system")
include("imp-auth")
include("imp-flaw")
include("imp-backend-service")
