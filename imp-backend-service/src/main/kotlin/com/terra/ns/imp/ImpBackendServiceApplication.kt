package com.terra.ns.imp

import cn.hutool.extra.spring.SpringUtil
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup
import org.springframework.boot.runApplication

/**
@author qins
 */
@SpringBootApplication
class LpSystemApplication

fun main(args: Array<String>) {
    runApplication<LpSystemApplication>(*args) { SpringApplication().applicationStartup = BufferingApplicationStartup(2048) }
    println("================================综合管理平台后台服务服务启动完成 当前环境 ${SpringUtil.getActiveProfile()}=====================================")
}