apply {
    plugin(projectLibs.plugins.springboot.get().pluginId)
}

dependencies{
    implementation(project(":imp-system"))
    implementation(project(":imp-flaw"))
    implementation(project(":imp-common:imp-common-web"))
}