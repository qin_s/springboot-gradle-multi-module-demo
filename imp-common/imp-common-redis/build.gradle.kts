dependencies {
    api(projectLibs.spring.redisson)
    api(project(":imp-common:imp-common-util"))
}