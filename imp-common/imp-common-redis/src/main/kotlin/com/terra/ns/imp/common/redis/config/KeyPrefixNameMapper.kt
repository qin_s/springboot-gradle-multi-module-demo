package com.terra.ns.imp.common.redis.config

import org.redisson.api.NameMapper

/**
@author qins
@date 2023/5/30
@desc 统一redis key前缀
 */
class KeyPrefixNameMapper(prefix: String) : NameMapper {

    private val prefix: String

    init {
        this.prefix = if(prefix.isBlank()) "" else "$prefix:"
    }

    override fun map(name: String) =
       if(prefix.isNotBlank() && !name.startsWith(prefix)) prefix + name else name

    override fun unmap(name: String) =
        if(prefix.isNotBlank() && name.startsWith(prefix)) name.substring(prefix.length) else name
}