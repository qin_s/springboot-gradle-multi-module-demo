package com.terra.ns.imp.common.redis.config

import org.redisson.config.Config
import org.redisson.spring.starter.RedissonAutoConfigurationCustomizer
import org.springframework.beans.factory.annotation.Value


/**
@author qins
@date 2023/5/30
@desc 给Redisson的config添加KeyPrefixNameMapper
 */
class KeyPrefixRedissonAutoConfigurationCustomizer: RedissonAutoConfigurationCustomizer {

    @Value("\${spring.redis.key-prefix}")
    private val prefix = ""

    override fun customize(cfg: Config) {
        val nameMapper = KeyPrefixNameMapper(prefix)
        if(cfg.isClusterConfig) {
            cfg.useClusterServers().setNameMapper(nameMapper)
        } else if (cfg.isSentinelConfig) {
            cfg.useClusterServers().setNameMapper(nameMapper)
        } else {
            cfg.useSingleServer().setNameMapper(nameMapper)
        }
    }
}