dependencies {
    implementation(projectLibs.aliyun.sms)
    implementation(project(":imp-common:imp-common-core"))
    implementation(project(":imp-common:imp-common-vo"))
    implementation("org.springframework.boot:spring-boot-actuator-autoconfigure")
    implementation(projectLibs.hutool.json)
}