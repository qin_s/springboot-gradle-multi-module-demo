package com.terra.ns.imp.common.sms

/**
@author qins
@date 2023/6/7
@desc 定义使用短信模板发送
 */
interface SmsTemplate {

    /**
     * 发送短信
     *
     * @param phones     电话号(多个逗号分割)
     * @param templateId 短信平台的模板id
     * @param param      模板对应参数
     */
    fun send(phones: String, templateId: String, param: Map<String, String>?)
}