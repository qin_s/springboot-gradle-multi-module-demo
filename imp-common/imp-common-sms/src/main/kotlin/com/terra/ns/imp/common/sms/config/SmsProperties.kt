package com.terra.ns.imp.common.sms.config

import org.springframework.boot.context.properties.ConfigurationProperties

/**
@author qins
@date 2023/6/7
@desc 短信配置属性
 */
@ConfigurationProperties(prefix = "sms")
class SmsProperties {

    // 是否支持短信
    var enable  = false

    /**
     * 云节点
     */
    var endpoint: String = ""

    /**
     * 访问key
     */
    var accessKeyId: String = ""

    /**
     * 访问Secret
     */
    var accessKeySecret: String = ""

    /**
     * 短信签名
     */
    var signName: String = ""

    /**
     * 针对验证码类的过期时长，单位 秒
     */
    var expireTime: Long = 300L

}