package com.terra.ns.imp.common.sms.config

import com.aliyun.dysmsapi20170525.Client
import com.terra.ns.imp.common.sms.AliyunSmsTemplate
import com.terra.ns.imp.common.sms.SmsTemplate
import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean

/**
@author qins
@date 2023/6/7
@desc 短信相关bean配置
 */
@AutoConfiguration
@EnableConfigurationProperties(SmsProperties::class)
class SmsConfiguration {

    @ConditionalOnProperty(value = ["sms.enable"], havingValue = "true")
    @ConditionalOnClass(Client::class)
    @Bean
    fun aliyunSmsTemplate(smsProperties: SmsProperties): SmsTemplate {
        return AliyunSmsTemplate(smsProperties)
    }
    // 其他短信商配置
}