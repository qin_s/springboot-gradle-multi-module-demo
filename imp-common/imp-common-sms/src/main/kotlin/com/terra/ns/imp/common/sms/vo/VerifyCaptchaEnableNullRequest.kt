package com.terra.ns.imp.common.sms.vo

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/6/7
@desc 校验验证码-可为空
 */
@Schema(description = "验证码校验请求")
open class VerifyCaptchaEnableNullRequest(

    @Schema(description = "短信验证码uuid，如果需要")
    var smsCodeUuid: String? = null,

    @Schema(description = "短信验证码， 如果需要")
    var smsCode: String? = null,

)