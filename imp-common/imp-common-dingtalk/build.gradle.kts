dependencies {
    implementation(projectLibs.dingtalk.stream){
        exclude("com.alibaba", "fastjson")
    }
    implementation(projectLibs.hutool.extra)
    api(projectLibs.dingtalk.sdk)
    api(projectLibs.bundles.fastjson)
    implementation("org.springframework.boot:spring-boot-autoconfigure")
}