package com.terra.ns.imp.common.dingtalk.event.model

/**
@author qins
@date 2023/9/19
@desc 钉钉文档 https://open.dingtalk.com/document/orgapp/approval-task-starts-ends-and-transfers-to-stream
 */
class BpmsTaskModel : BpmsModel() {

    var taskId: Long = 0L


    companion object {
        const val TYPE_START = "start"
        const val TYPE_FINISH = "finish"
        const val TYPE_CANCEL = "cancel"
        const val RESULT_AGREE = "agree"
        const val RESULT_REFUSE = "refuse"
        const val RESULT_REDIRECT = "redirect"
    }
}