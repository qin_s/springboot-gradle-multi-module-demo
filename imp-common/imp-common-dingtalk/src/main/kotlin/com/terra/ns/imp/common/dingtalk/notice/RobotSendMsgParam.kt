package com.gzca.digit.common.dingtalk.notice

/**
@author qins
@date 2023/11/15
@desc 使用机器人发送消息参数
 */
class RobotSendMsgParam {

    /**
     * 消息模板标识 参考https://open.dingtalk.com/document/group/message-types-and-data-format
     */
    var msgKey: String = ""

    /**
     * 消息参数 参考https://open.dingtalk.com/document/group/message-types-and-data-format
     */
    var msgParam: String = ""
}