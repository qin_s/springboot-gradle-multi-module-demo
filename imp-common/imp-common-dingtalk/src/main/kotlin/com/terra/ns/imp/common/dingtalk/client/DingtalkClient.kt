package com.terra.ns.imp.common.dingtalk.client

import com.aliyun.teaopenapi.models.Config
import com.terra.ns.imp.common.dingtalk.config.DingtalkProperties



/**
@author qins
@date 2023/8/16
@desc 钉钉开发常用请求客户端
 */
class DingtalkClient(private var dingTalkProperties: DingtalkProperties) {

    private fun getBaseConfig() : Config {
        val config = Config()
        config.accessKeyId = dingTalkProperties.appKey
        config.accessKeySecret = dingTalkProperties.appSecret
        config.protocol = "https"
        config.regionId = "central"
        return config
    }

    fun getDingtalkConf() : DingtalkProperties = dingTalkProperties

    fun authClient() : com.aliyun.dingtalkoauth2_1_0.Client = com.aliyun.dingtalkoauth2_1_0.Client(getBaseConfig())
    fun contactClient() : com.aliyun.dingtalkcontact_1_0.Client = com.aliyun.dingtalkcontact_1_0.Client(getBaseConfig())
    fun workflowClient() : com.aliyun.dingtalkworkflow_1_0.Client = com.aliyun.dingtalkworkflow_1_0.Client(getBaseConfig())
    fun storageClient() : com.aliyun.dingtalkstorage_2_0.Client = com.aliyun.dingtalkstorage_2_0.Client(getBaseConfig())
    fun storageV1Client() : com.aliyun.dingtalkstorage_1_0.Client = com.aliyun.dingtalkstorage_1_0.Client(getBaseConfig())
    fun robotClient() : com.aliyun.dingtalkrobot_1_0.Client  = com.aliyun.dingtalkrobot_1_0.Client(getBaseConfig())
}