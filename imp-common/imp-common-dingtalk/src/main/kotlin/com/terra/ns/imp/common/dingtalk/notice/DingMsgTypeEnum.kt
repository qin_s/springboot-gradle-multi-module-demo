package com.terra.ns.imp.common.dingtalk.notice

/**
@author qins
@date 2022/11/30
@desc 钉钉消息类型枚举
 */
enum class DingMsgTypeEnum(val type: String, val desc: String) {

    TEXT("text", "普通文本消息"),
    MARKDOWN("markdown", "markdown消息"),
    SAMPLE_MARKDOWN("sampleMarkdown", "markdown消息"),
    LINK("link", "链接消息"),
    ACTION_CARD("action_card", "卡片消息"),
    SAMPLE_ACTION_CARD("sampleActionCard", "卡片消息"),
    FILE("file", "文件消息"),
    VOICE("voice", "语音消息"),
    IMAGE("image", "图片消息");

    companion object {
        fun getDesc(type: String?) : String? = type?.let { values().firstOrNull { it.type == type } }?.desc

    }
}