package com.terra.ns.imp.common.dingtalk.config

import org.springframework.boot.context.properties.ConfigurationProperties

/**
@author qins
@date 2023/8/16
@desc 统一钉钉配置项
 */
@ConfigurationProperties(prefix = "dingtalk")
class DingtalkProperties {

    /**
     * 钉钉接口相关属性
     */
    var appKey: String? = null

    var appSecret: String? = null

    var agentId: String? = null

    var corpId: String? = null
}