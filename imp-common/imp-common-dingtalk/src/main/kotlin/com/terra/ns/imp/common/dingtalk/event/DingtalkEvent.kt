package com.terra.ns.imp.common.dingtalk.event

import com.alibaba.fastjson2.JSONObject
import org.springframework.context.ApplicationEvent


/**
@author qins
@date 2023/9/19
@desc
 */
class DingtalkEvent(
    var eventId: String,
    var eventType: String,
    var eventCorpId: String,
    var eventBornTime: Long,
    var data: JSONObject,
    source: Any?) :
    ApplicationEvent(source ?: "auto")