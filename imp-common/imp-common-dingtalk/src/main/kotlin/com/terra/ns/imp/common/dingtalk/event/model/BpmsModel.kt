package com.terra.ns.imp.common.dingtalk.event.model

/**
@author qins
@date 2023/9/19
@desc
 */
open class BpmsModel {

    var processInstanceId: String = ""
    var finishTime: Long? = null
    var createTime: Long = 0L
    var processCode: String = ""
    var bizCategoryId: String = ""
    var businessId: String = ""
    var remark: String = ""
    var type: String = ""
    var title: String = ""
    var staffId: String = ""
    var result: String = ""
}