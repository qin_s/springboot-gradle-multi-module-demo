package com.terra.ns.imp.common.dingtalk.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

/**
@author qins
@date 2023/8/16
@desc 钉钉消息配置项
 */
@Configuration
@ConfigurationProperties(prefix = "dingtalk.msg")
class DingtalkMsgProperties {

    var robotCode: String? = null

    var openConversationId: String? = null

    var coolAppCode: String? = null
}