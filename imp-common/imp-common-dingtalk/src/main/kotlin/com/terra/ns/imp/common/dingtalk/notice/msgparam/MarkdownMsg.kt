package com.terra.ns.imp.common.dingtalk.notice.msgparam

/**
@author qins
@date 2023/11/15
@desc
 */
class MarkdownMsg {

    var title: String = ""

    var text: String = ""
}