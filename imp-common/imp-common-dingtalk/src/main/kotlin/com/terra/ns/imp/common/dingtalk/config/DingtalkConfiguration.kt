package com.terra.ns.imp.common.dingtalk.config

import com.terra.ns.imp.common.dingtalk.client.DingtalkClient
import com.terra.ns.imp.common.dingtalk.service.DingAuthService
import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean

/**
@author qins
@date 2023/8/16
@desc
 */
@AutoConfiguration
@EnableConfigurationProperties(DingtalkProperties::class)
class DingtalkConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = "dingtalk", name = ["app-key", "app-secret"])
    fun dingTalkClient(properties: DingtalkProperties) : DingtalkClient {
        return DingtalkClient(properties)
    }

    @Bean
    @ConditionalOnBean(value = [DingtalkClient::class])
    fun dingAuthService() : DingAuthService {
        return DingAuthService()
    }
}