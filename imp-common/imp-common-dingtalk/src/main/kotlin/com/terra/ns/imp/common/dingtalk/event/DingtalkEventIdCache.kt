package com.terra.ns.imp.common.dingtalk.event

import java.util.concurrent.ConcurrentHashMap

/**
@author qins
@date 2023/9/19
@desc
 */
object DingtalkEventIdCache {

    private val handingEventIds = ConcurrentHashMap.newKeySet<String>()

    fun add(eventId: String) {
        handingEventIds.add(eventId)
    }

    fun size() : Int {
        return handingEventIds.size
    }


    fun clear() {
        handingEventIds.clear()
    }

    fun del(eventId: String) {
        handingEventIds.remove(eventId)
    }

    fun contains(eventId: String) : Boolean = handingEventIds.contains(eventId)

}