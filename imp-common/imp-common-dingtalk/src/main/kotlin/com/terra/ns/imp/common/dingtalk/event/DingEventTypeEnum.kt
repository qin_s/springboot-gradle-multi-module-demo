package com.terra.ns.imp.common.dingtalk.event

/**
@author qins
@date 2023/9/19
@desc 钉钉事件类型，要新增则添加枚举，然后在@AbstractDingtalkEventListener 定义事件处理
https://open.dingtalk.com/document/isvapp/hidden-event-list
 事件详细： https://open.dingtalk.com/document/orgapp/address-book-user-add-stream
 */
enum class DingEventTypeEnum(var type: String) {

    // 通讯录用户增加
    USER_ADD_ORG("user_add_org"),

    // 通讯录用户更改
    USER_MODIFY_ORG("user_modify_org"),

    // 通讯录用户离职
    USER_LEAVE_ORG("user_leave_org"),

     // 加入企业后用户激活
     USER_ACTIVE_ORG("user_active_org"),

    // 通讯录用户被设为管理员
    ORG_ADMIN_ADD("org_admin_add"),

    // 通讯录用户被取消设置管理员。
    ORG_ADMIN_REMOVE("org_admin_remove"),

    // 通讯录企业部门创建
    ORG_DEPT_CREATE("org_dept_create"),

    // 通讯录企业部门修改
    ORG_DEPT_MODIFY("org_dept_modify"),

    // 通讯录企业部门删除
    ORG_DEPT_REMOVE("org_dept_remove"),

    // 企业被解散
    ORG_REMOVE("org_remove"),

    // 企业信息发生变更
    ORG_CHANGE("org_change"),

    // 员工角色信息发生变更
    LABEL_USER_CHANGE("label_user_change"),

    // 增加角色或者角色组
    LABEL_CONF_ADD("label_conf_add"),

    // 删除角色或者角色组
    LABEL_CONF_DEL("label_conf_del"),

    // 修改角色或者角色组
    LABEL_CONF_MODIFY("label_conf_modify"),

    // 员工打卡事件
    ATTENDANCE_CHECK_RECORD("attendance_check_record"),

    // 员工排班变更事件
    ATTENDANCE_SCHEDULE_CHANGE("attendance_schedule_change"),

    // 员工加班事件
    ATTENDANCE_OVERTIME_DURATION("attendance_overtime_duration"),

    // 用户签到事件
    CHECK_IN("check_in"),


    // 审批任务开始，结束，转交
    BPMS_TASK_CHANGE("bpms_task_change"),
    //审批实例开始，结束
    BPMS_INSTANCE_CHANGE("bpms_instance_change"),

    // ...
    ;
    companion object {

        fun enumOf(type: String) : DingEventTypeEnum? =
            values().firstOrNull { it.type == type }
    }
}