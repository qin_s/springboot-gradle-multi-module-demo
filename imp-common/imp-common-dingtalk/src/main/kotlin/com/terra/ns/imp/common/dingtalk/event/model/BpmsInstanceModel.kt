package com.terra.ns.imp.common.dingtalk.event.model


/**
@author qins
@date 2023/9/19
@desc 钉钉文档 https://open.dingtalk.com/document/orgapp/approve-instance-start-end-stream
 */
class BpmsInstanceModel : BpmsModel() {

    var businessType: String = ""

    var url: String = ""

    companion object {

        const val TYPE_START = "start"
        const val TYPE_FINISH = "finish"
        const val TYPE_TERMINATE = "terminate"
        const val RESULT_AGREE = "agree"
        const val RESULT_REFUSE = "refuse"


    }
}