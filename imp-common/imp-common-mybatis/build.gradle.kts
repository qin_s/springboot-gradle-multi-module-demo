dependencies {
    api(projectLibs.mybatis.plus)
    implementation(project(":imp-common:imp-common-util"))
    implementation(project(":imp-common:imp-common-satoken"))
    implementation(project(":imp-common:imp-common-vo"))
}