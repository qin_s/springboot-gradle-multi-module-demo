package com.terra.ns.imp.common.mybatis.config

import com.baomidou.mybatisplus.annotation.DbType
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor
import com.terra.ns.imp.common.mybatis.ext.SqlInjectorX
import org.mybatis.spring.annotation.MapperScan
import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.transaction.annotation.EnableTransactionManagement

/**
@author qins
@date 2023/5/31
@desc mybatis plus 配置
 */
@AutoConfiguration
@EnableTransactionManagement(proxyTargetClass = true)
@MapperScan("\${mybatis-plus.mapper-packages}")
class MyBatisPlusConfig {

    @Bean
    fun mybatisPlusInterceptor(): MybatisPlusInterceptor {
        val mybatisPlusInterceptor = MybatisPlusInterceptor()
        // 分页插件
        mybatisPlusInterceptor.addInnerInterceptor(PaginationInnerInterceptor(DbType.MYSQL))
        // 乐观锁插件
        mybatisPlusInterceptor.addInnerInterceptor(OptimisticLockerInnerInterceptor())
        return mybatisPlusInterceptor
    }

    @Bean
    fun sqlInjectorX(): SqlInjectorX = SqlInjectorX()

    @Bean
    fun autoFillMetaObjectHandler() = AutoFillMetaObjectHandler()

}