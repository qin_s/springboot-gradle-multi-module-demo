package com.terra.ns.imp.common.mybatis.base

import com.terra.ns.imp.common.core.definition.ConstantDefinition

object MpConstants : ConstantDefinition {

    const val SYSTEM_OPERATOR = "system" // 创建人/更新人为系统的编码
    const val SYSTEM_OPERATOR_NAME = "系统" // 创建人/更新人为系统的名称
}