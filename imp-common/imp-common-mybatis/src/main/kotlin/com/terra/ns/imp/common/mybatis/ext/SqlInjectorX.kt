package com.terra.ns.imp.common.mybatis.ext

import com.baomidou.mybatisplus.annotation.FieldFill
import com.baomidou.mybatisplus.core.injector.AbstractMethod
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector
import com.baomidou.mybatisplus.core.metadata.TableFieldInfo
import com.baomidou.mybatisplus.core.metadata.TableInfo
import com.baomidou.mybatisplus.extension.injector.methods.InsertBatchSomeColumn


/**
@author qins
@date 2023/5/31
@desc 添加自定义sql方法
 */
class SqlInjectorX : DefaultSqlInjector() {

    override fun getMethodList(mapperClass: Class<*>?, tableInfo: TableInfo?): MutableList<AbstractMethod> {
        val methodList = super.getMethodList(mapperClass, tableInfo)
        // 添加批量插入
        methodList.add(InsertBatchSomeColumn { getFilterUpdateFieldIncludeNull(it) })
        // 添加包含null内容的更新
        methodList.add(UpdateFieldIncludeNull { getFilterUpdateFieldIncludeNull(it) })
        return methodList
    }

    private fun getFilterUpdateFieldIncludeNull(tbInfo: TableFieldInfo) : Boolean {
        return !tbInfo.isLogicDelete
                && tbInfo.fieldFill != FieldFill.INSERT_UPDATE
                && tbInfo.fieldFill != FieldFill.INSERT
    }
}