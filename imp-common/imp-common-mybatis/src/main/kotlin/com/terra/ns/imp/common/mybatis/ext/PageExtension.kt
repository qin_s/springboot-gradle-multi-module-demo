package com.terra.ns.imp.common.mybatis.ext

import com.baomidou.mybatisplus.extension.plugins.pagination.Page
import com.terra.ns.imp.common.vo.response.PageResult

/**
@author qins
@date 2023/6/27
@desc 分页插件结果的扩展操作
 */
// mp的分页结果
fun <T> Page<T>.isEmpty() = total <= 0 || records.isNullOrEmpty()
fun <T> Page<T>.isNotEmpty() = !isEmpty()

/**
 * 将Mp分页结果对象的实体列表转化为统一分页结果
 */
fun <T, R> Page<T>.convertToResponse(response: List<R>): PageResult<R> {
    return PageResult(this.current, this.size, this.total, response)
}

/**
 * 将Mp分页结果对象的实体列表转化为统一分页结果
 */
fun <T> Page<T>.convertToResponse(): PageResult<T> {
    return PageResult(this.current, this.size, this.total, this.records)
}
