dependencies {
    implementation(projectLibs.dingtalk.service)
    implementation(projectLibs.bundles.fastjson)
    implementation(projectLibs.mybatis.plus)
    implementation(projectLibs.hutool.crypto)
    implementation(project(":imp-common:imp-common-vo"))
    implementation(project(":imp-common:imp-common-dingtalk"))
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
}