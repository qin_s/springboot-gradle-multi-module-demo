package com.terra.ns.imp.common.dingtalk.contact.domain;

import java.io.Serializable;
import java.util.Date;

public class DepartmentDetailDTO implements Serializable {
    private Long id;

    /**
     * 钉钉部门ID
     */
    private Long deptId;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 父部门ID（为1表示为根部门）
     */
    private Long parentid;

    /**
     * 部门唯一自定义标识（可使用统一编码关联）
     */
    private String sourceIdentifier;

    /**
     * 是否同步创建一个关联此部门的企业群：0：不创建 ; 1：创建
     */
    private Integer createDeptGroup;

    /**
     * 当部门群已经创建后，是否有新人加入部门会自动加入该群：0:不加入；1,加入
     */
    private Integer autoAddUser;

    /**
     * 是否默认同意加入该部门的申请：0，需审批；1，默认同意
     */
    private Integer autoApproveApply;

    /**
     * 部门是否来自关联组织:0：否；1，是
     */
    private Integer fromUnionOrg;

    /**
     * 教育部门标签：campus：校区;period：学段;grade：年级;class：班级
     */
    private String tags;

    /**
     * 在父部门中的次序值(小者靠前)
     */
    private Long order;

    /**
     * 部门群ID。
     */
    private String deptGroupChatId;

    /**
     * 部门群是否包含子部门：0：否；1：是
     */
    private Integer groupContainSubDept;

    /**
     * 企业群群主userId。
     */
    private String orgDeptOwner;

    /**
     * 部门主管userId列表（以"，"分割）
     */
    private String deptManagerUseridList;

    /**
     * 是否限制本部门成员查看通讯录：0:不限制；1：限制只能看限定范围内的通讯录
     */
    private Integer outerDept;

    /**
     * OUTER_DEPT = 1时，部门员工可见部门ID列表，（以"，"分割）
     */
    private String outerPermitDepts;

    /**
     * OUTER_DEPT = 1时，部门员工可见员工ID列表，（以"，"分割）
     */
    private String outerPermitUsers;

    /**
     * 是否隐藏本部门：0:，不隐藏；1：隐藏（不显示在公司通讯录）
     */
    private Integer hideDept;

    /**
     * HIDE_DEPT = 1时，允许查看本部门的用户ID列表
     */
    private String userPermits;

    /**
     * HIDE_DEPT = 1时，允许查看本部门的部门ID列表
     */
    private String deptPermits;

    /**
     * 是否删除（0,未删除；1，已删除）
     */
    private Integer deleted;

    /**
     * 备用字段1
     */
    private String ext1;

    /**
     * 备用字段2
     */
    private String ext2;

    /**
     * 备用字段3
     */
    private String ext3;

    /**
     * 备用字段4
     */
    private String ext4;

    /**
     * 备用字段5
     */
    private String ext5;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 变更时间
     */
    private Date modifyDate;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    public String getSourceIdentifier() {
        return sourceIdentifier;
    }

    public void setSourceIdentifier(String sourceIdentifier) {
        this.sourceIdentifier = sourceIdentifier;
    }

    public Integer getCreateDeptGroup() {
        return createDeptGroup;
    }

    public void setCreateDeptGroup(Integer createDeptGroup) {
        this.createDeptGroup = createDeptGroup;
    }

    public Integer getAutoAddUser() {
        return autoAddUser;
    }

    public void setAutoAddUser(Integer autoAddUser) {
        this.autoAddUser = autoAddUser;
    }

    public Integer getAutoApproveApply() {
        return autoApproveApply;
    }

    public void setAutoApproveApply(Integer autoApproveApply) {
        this.autoApproveApply = autoApproveApply;
    }

    public Integer getFromUnionOrg() {
        return fromUnionOrg;
    }

    public void setFromUnionOrg(Integer fromUnionOrg) {
        this.fromUnionOrg = fromUnionOrg;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public String getDeptGroupChatId() {
        return deptGroupChatId;
    }

    public void setDeptGroupChatId(String deptGroupChatId) {
        this.deptGroupChatId = deptGroupChatId;
    }

    public Integer getGroupContainSubDept() {
        return groupContainSubDept;
    }

    public void setGroupContainSubDept(Integer groupContainSubDept) {
        this.groupContainSubDept = groupContainSubDept;
    }

    public String getOrgDeptOwner() {
        return orgDeptOwner;
    }

    public void setOrgDeptOwner(String orgDeptOwner) {
        this.orgDeptOwner = orgDeptOwner;
    }

    public String getDeptManagerUseridList() {
        return deptManagerUseridList;
    }

    public void setDeptManagerUseridList(String deptManagerUseridList) {
        this.deptManagerUseridList = deptManagerUseridList;
    }

    public Integer getOuterDept() {
        return outerDept;
    }

    public void setOuterDept(Integer outerDept) {
        this.outerDept = outerDept;
    }

    public String getOuterPermitDepts() {
        return outerPermitDepts;
    }

    public void setOuterPermitDepts(String outerPermitDepts) {
        this.outerPermitDepts = outerPermitDepts;
    }

    public String getOuterPermitUsers() {
        return outerPermitUsers;
    }

    public void setOuterPermitUsers(String outerPermitUsers) {
        this.outerPermitUsers = outerPermitUsers;
    }

    public Integer getHideDept() {
        return hideDept;
    }

    public void setHideDept(Integer hideDept) {
        this.hideDept = hideDept;
    }

    public String getUserPermits() {
        return userPermits;
    }

    public void setUserPermits(String userPermits) {
        this.userPermits = userPermits;
    }

    public String getDeptPermits() {
        return deptPermits;
    }

    public void setDeptPermits(String deptPermits) {
        this.deptPermits = deptPermits;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1;
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2;
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3;
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4;
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }
}