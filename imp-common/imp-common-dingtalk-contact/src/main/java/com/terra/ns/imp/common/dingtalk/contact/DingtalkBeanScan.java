package com.terra.ns.imp.common.dingtalk.contact;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @desc 描述注入钉钉相关bean
 */
@AutoConfiguration
@ComponentScan(basePackages = {
        "com.terra.ns.imp.common.dingtalk.contact.*"
})
public class DingtalkBeanScan{

}
