package com.terra.ns.imp.common.dingtalk.contact.domain;



import java.util.List;

public class UserDetail {

    private Boolean active;
    private Boolean admin;
    private String avatar;
    private Boolean boss;

    private List<Long> deptIdList;

    private List<DeptPosition> deptPositionList ;

    private String email;

    private Boolean exclusiveAccount;

    private String exclusiveAccountType;

    private String extension;

    private Boolean hideMobile;

    private Long hiredDate;

    private String jobNumber;

    private List<DeptLeader> leaderInDept;

    private String loginId;
    private String managerUserid;

    private String mobile;

    private String name;

    private String orgEmail;

    private String orgEmailType;

    private Boolean realAuthed;

    private String remark;
    private List<UserRole> roleList;
    private Boolean senior;
    private String stateCode;
    private String telephone;
    private String title;

    private String unionid;

    private String userid;

    private String workPlace;



    public Boolean getActive() {
        return this.active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getAdmin() {
        return this.admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Boolean getBoss() {
        return this.boss;
    }

    public void setBoss(Boolean boss) {
        this.boss = boss;
    }

    public List<Long> getDeptIdList() {
        return this.deptIdList;
    }

    public void setDeptIdList(List<Long> deptIdList) {
        this.deptIdList = deptIdList;
    }



    public List<DeptPosition> getDeptPositionList() {
        return this.deptPositionList;
    }

    public void setDeptPositionList(List<DeptPosition> deptPositionList) {
        this.deptPositionList = deptPositionList;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getExclusiveAccount() {
        return this.exclusiveAccount;
    }

    public void setExclusiveAccount(Boolean exclusiveAccount) {
        this.exclusiveAccount = exclusiveAccount;
    }

    public String getExclusiveAccountType() {
        return this.exclusiveAccountType;
    }

    public void setExclusiveAccountType(String exclusiveAccountType) {
        this.exclusiveAccountType = exclusiveAccountType;
    }

    public String getExtension() {
        return this.extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Boolean getHideMobile() {
        return this.hideMobile;
    }

    public void setHideMobile(Boolean hideMobile) {
        this.hideMobile = hideMobile;
    }

    public Long getHiredDate() {
        return this.hiredDate;
    }

    public void setHiredDate(Long hiredDate) {
        this.hiredDate = hiredDate;
    }

    public String getJobNumber() {
        return this.jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public List<DeptLeader> getLeaderInDept() {
        return this.leaderInDept;
    }

    public void setLeaderInDept(List<DeptLeader> leaderInDept) {
        this.leaderInDept = leaderInDept;
    }

    public String getLoginId() {
        return this.loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getManagerUserid() {
        return this.managerUserid;
    }

    public void setManagerUserid(String managerUserid) {
        this.managerUserid = managerUserid;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrgEmail() {
        return this.orgEmail;
    }

    public void setOrgEmail(String orgEmail) {
        this.orgEmail = orgEmail;
    }

    public String getOrgEmailType() {
        return this.orgEmailType;
    }

    public void setOrgEmailType(String orgEmailType) {
        this.orgEmailType = orgEmailType;
    }

    public Boolean getRealAuthed() {
        return this.realAuthed;
    }

    public void setRealAuthed(Boolean realAuthed) {
        this.realAuthed = realAuthed;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<UserRole> getRoleList() {
        return this.roleList;
    }

    public void setRoleList(List<UserRole> roleList) {
        this.roleList = roleList;
    }

    public Boolean getSenior() {
        return this.senior;
    }

    public void setSenior(Boolean senior) {
        this.senior = senior;
    }

    public String getStateCode() {
        return this.stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public String getUnionid() {
        return this.unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getWorkPlace() {
        return this.workPlace;
    }

    public void setWorkPlace(String workPlace) {
        this.workPlace = workPlace;
    }
}
