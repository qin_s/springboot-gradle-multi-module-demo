package com.terra.ns.imp.common.dingtalk.contact.domain;

import com.taobao.api.internal.mapping.ApiField;

public class DeptLeader {
    @ApiField("dept_id")
    private Long deptId;
    @ApiField("leader")
    private Boolean leader;

    public DeptLeader() {
    }

    public Long getDeptId() {
        return this.deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Boolean getLeader() {
        return this.leader;
    }

    public void setLeader(Boolean leader) {
        this.leader = leader;
    }
}
