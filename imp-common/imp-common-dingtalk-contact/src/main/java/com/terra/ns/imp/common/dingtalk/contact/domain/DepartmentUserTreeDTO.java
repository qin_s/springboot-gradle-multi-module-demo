package com.terra.ns.imp.common.dingtalk.contact.domain;

import java.util.ArrayList;
import java.util.List;

public class DepartmentUserTreeDTO extends DepartmentUserDTO{

    private List<DepartmentUserTreeDTO> children = new ArrayList<>();

    public List<DepartmentUserTreeDTO> getChildren() {
        return children;
    }

    public void setChildren(List<DepartmentUserTreeDTO> children) {
        this.children = children;
    }
}
