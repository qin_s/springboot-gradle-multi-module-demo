package com.terra.ns.imp.common.dingtalk.contact.domain;

import java.util.ArrayList;
import java.util.List;

public class DepartmentTreeDTO extends DepartmentDTO {

    private List<DepartmentTreeDTO> children = new ArrayList<>();

    public List<DepartmentTreeDTO> getChildren() {
        return children;
    }

    public void setChildren(List<DepartmentTreeDTO> children) {
        this.children = children;
    }
}
