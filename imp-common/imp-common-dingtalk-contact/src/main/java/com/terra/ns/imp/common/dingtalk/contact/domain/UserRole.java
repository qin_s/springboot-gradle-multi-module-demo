package com.terra.ns.imp.common.dingtalk.contact.domain;

public class UserRole {

    private static final long serialVersionUID = 4184981891363852626L;
    private String groupName;
    private Long id;
    private String name;

    public UserRole() {
    }

    public String getGroupName() {
        return this.groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
