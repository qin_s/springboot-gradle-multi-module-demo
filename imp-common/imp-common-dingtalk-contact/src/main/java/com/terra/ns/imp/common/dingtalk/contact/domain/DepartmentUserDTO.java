package com.terra.ns.imp.common.dingtalk.contact.domain;


import java.util.ArrayList;
import java.util.List;

public class DepartmentUserDTO extends DepartmentDTO {

    // 用户列表
    private List<UserV2DTO> userList = new ArrayList<UserV2DTO>();

    public List<UserV2DTO> getUserList() {
        return userList;
    }

    public void setUserList(List<UserV2DTO> userList) {
        this.userList = userList;
    }
}
