package com.terra.ns.imp.common.log

/**
@author qins
@date 2023/6/2
@desc 操作日志操作类型枚举
 */
enum class OperateTypeEnum(val type: String, val desc: String){

    ADD("ADD", "新增"),
    DELETE("DELETE", "删除"),
    MODIFY("MODIFY", "修改"),
    QUERY("QUERY", "查询"),
    LOGIN("LOGIN", "登录"),
    LOGOUT("LOGOUT", "登出"),
    REGISTER("REGISTER", "注册"),
    IMPORT("IMPORT", "导入"),
    EXPORT("EXPORT", "导出"),
    OTHER("OTHER", "其它"),
    UNKNOWN("UNKNOWN", "未知");
    companion object {
        fun getDesc(type: String?): String? {
            type?.let {
                return values().firstOrNull { it.type == type }?.desc
            }
            return UNKNOWN.desc
        }
    }
}
