package com.terra.ns.imp.common.log

import cn.hutool.core.bean.BeanUtil
import com.terra.ns.imp.system.api.entity.StOperateLog
import com.terra.ns.imp.system.api.service.IOperateLogApi
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

/**
@author qins
@date 2023/6/3
@desc 处理操作日志
 */
@Component
class OperateLogEventListener{

    @Autowired
    private lateinit var operateLogApiService: IOperateLogApi

    @Async
    @EventListener
    fun saveLog(logEvent: OperateLogEvent) {
        val saveLog = StOperateLog()
        BeanUtil.copyProperties(logEvent, saveLog)
        operateLogApiService.saveOperateLog(saveLog)
    }

}