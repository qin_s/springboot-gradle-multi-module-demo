package com.terra.ns.imp.common.log

/**
@author qins
@date 2023/6/3
@desc 操作结果
 */
enum class OperateResultEnum(val result: String, val desc: String) {

    SUCCESS("SUCCESS", "成功"),
    FAIL("FAIL", "失败")

}