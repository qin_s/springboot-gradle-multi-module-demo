package com.terra.ns.imp.common.log

import java.io.Serializable
import java.time.LocalDateTime

/**
@author qins
@date 2023/6/2
@desc
 */
class OperateLogEvent: Serializable {

    var traceId: String? = null
    /**
     * 操作账号
     */
    var operatorAccount: String? = null
    /**
     * 操作者名称
     */
    var operatorName: String? = null
    /**
     * 操作者IP
     */
    var operatorIp: String? = null
    /**
     * 模块名称
     */
    var moduleName: String? = null
    /**
     * 请求地址
     */
    var requestUrl: String? = null

    /**
     * 请求方法
     */
    var requestMethod: String? = null
    /**
     * 请求参数
     */
    var requestParam: String? = null
    /**
     * 操作类型枚举
     */
    var operateType: String? = null

    /**
     * 响应数据
     */
    var responseData: String? = null

    /**
     * 操作结果
     */
    var operateResult: String? = null

    /**
     * 提示信息
     */
    var resultMessage: String? = null

    /**
     * 操作时间
     */
    var operateTime: LocalDateTime? = null

    /**
     * 处理时长
     */
    var handleTime: Long? = null
}