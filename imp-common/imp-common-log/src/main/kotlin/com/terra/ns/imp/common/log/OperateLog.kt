package com.terra.ns.imp.common.log

/**
 * @author qins
 * @date 2023/6/2
 * @desc 标识需要保存操作日志的接口
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class OperateLog(
    /**
     * 模块
     */
    val module: String = "",

    /**
     * 操作类型
     */
    val operateType: OperateTypeEnum = OperateTypeEnum.UNKNOWN,

    /**
     * 描述信息
     */
    val desc: String = "",

    /**
     * 保存请求参数
     */
    val saveRequestParam: Boolean = true,

    /**
     * 保存响应数据
     */
    val saveResponseData: Boolean = true
)
