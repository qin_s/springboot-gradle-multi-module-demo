package com.terra.ns.imp.common.log

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

/**
@author qins
@date 2023/6/2
@desc 操作日志相关属性配置
 */
@ConfigurationProperties(prefix = "log.web.operate")
@Configuration
class OperateLogProperties {

    //打印日志
    var print = true

    //保存日志
    var save = true
}