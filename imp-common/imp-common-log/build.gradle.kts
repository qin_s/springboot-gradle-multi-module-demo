dependencies {

    implementation(project(":imp-common:imp-common-satoken"))
    implementation(project(":imp-common:imp-common-util"))
    implementation(project(":imp-api:imp-system-api"))
    implementation("org.springframework.boot:spring-boot-starter-aop")
    implementation("org.apache.tomcat.embed:tomcat-embed-core")
}