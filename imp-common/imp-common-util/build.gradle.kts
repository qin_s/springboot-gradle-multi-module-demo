
dependencies {
    api(projectLibs.bundles.hutool)
    api(project(":imp-common:imp-common-core"))
    implementation("org.apache.tomcat.embed:tomcat-embed-core")
}