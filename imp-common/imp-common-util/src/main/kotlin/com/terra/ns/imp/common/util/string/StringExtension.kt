package com.terra.ns.imp.common.util.string

/**
@author qins
@date 2023/7/6
@desc 字符串类型扩展操作
 */
/**
 * 如果为字符串null或blank返回默认值
 */
fun String?.nullOrBlankDefault(default: String = "") : String
    = if(this.isNullOrBlank()) default else this


/**
 * 如果为字符串null返回默认值
 */
fun String?.nullDefault(default: String = "") : String
        = this ?: default
