package com.terra.ns.imp.common.util.exception

import com.terra.ns.imp.common.core.exception.ServiceException

/**
@author qins
@date 2023/6/5
@desc 异常扩展操作
 */
/**
 * 判断执行方法是否会抛出ServiceException
 */
fun hasServiceException(func: Function0<Any>): Boolean {
    try {
        func.invoke()
    } catch (se: ServiceException) {
        return true
    }
    return false
}

fun withoutServiceException(func: Function0<Any>): Boolean {
    return !hasServiceException(func)
}

/**
 * 判断执行方法是否会抛出ServiceException， 有ServiceException异常直接返回null
 */
fun <T> hasServiceExceptionReturnNull(func: Function0<T>): T? {
    return try {
        func.invoke()
    } catch (se: ServiceException) {
        null
    }
}


/**
 * 判断执行方法是否会抛出异常，有异常直接返回null
 */
fun <T> hasExceptionReturnNull(func: Function0<T>): T? {
    return try {
        func.invoke()
    } catch (se: Exception) {
        null
    }
}
