package com.terra.ns.imp.common.satoken.custom

import cn.dev33.satoken.stp.StpInterface

/**
@author qins
@date 2023/6/1
@desc 自定义satoken的权限获取方式
 */
class CustomPermissionImpl : StpInterface {

    override fun getPermissionList(loginId: Any, loginType: String): List<String> {
        return  emptyList()
    }

    override fun getRoleList(loginId: Any, loginType: String): List<String> {
        return  emptyList()
    }
}