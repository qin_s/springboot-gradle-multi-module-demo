package com.terra.ns.imp.common.satoken.config

import cn.dev33.satoken.dao.SaTokenDao
import cn.dev33.satoken.stp.StpInterface
import com.terra.ns.imp.common.satoken.custom.CustomPermissionImpl
import com.terra.ns.imp.common.satoken.custom.RedissonTokenDao
import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Bean

/**
@author qins
@date 2023/6/1
@desc 配置satoken自定义实现bean
 */
@AutoConfiguration
class SaTokenConfig {

    @Bean
    fun saTokenDao() : SaTokenDao {
        return RedissonTokenDao()
    }

    @Bean
    fun tpInterface() : StpInterface {
        return CustomPermissionImpl()
    }
}