package com.terra.ns.imp.common.satoken.constant

/**
@author qins
@date 2023/6/1
@desc 登录设备类型
 */
enum class LoginDeviceType(val device: String, val desc: String) {

    WEB("web", "pc web端"),
    MOBILE("mobile", "移动端"),
}