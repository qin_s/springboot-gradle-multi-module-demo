
dependencies {
    api(projectLibs.satoken.core) // 仅依赖core
    implementation(project(":imp-common:imp-common-redis"))
    implementation(project(":imp-api:imp-system-api"))
}