dependencies {
    implementation(projectLibs.minio)
    implementation("org.springframework.boot:spring-boot")
    implementation("org.springframework.boot:spring-boot-autoconfigure")

}