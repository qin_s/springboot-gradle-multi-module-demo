package com.terra.ns.imp.common.oss.minio

import io.minio.MinioClient
import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean

/**
@author qins
@date 2023/8/11
@desc Minio客户端  要求配置属性"minio.endpoint", "minio.access-key", "minio.secret"
 */
@AutoConfiguration
@EnableConfigurationProperties(MinioProperties::class)
class MinioConfiguration {

    @Bean
    @ConditionalOnProperty(value = ["minio.endpoint", "minio.access-key", "minio.secret"])
    fun minioClient(properties: MinioProperties) : MinioClient {
        return MinioClient.builder().endpoint(properties.endpoint).credentials(properties.accessKey, properties.secret) .build()
    }

    @Bean
    @ConditionalOnBean(value = [MinioClient::class])
    fun miniUtils(client: MinioClient, properties: MinioProperties) : MinioUtils {
        return MinioUtils(client, properties)
    }
}