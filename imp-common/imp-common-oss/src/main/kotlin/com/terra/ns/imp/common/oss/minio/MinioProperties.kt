package com.terra.ns.imp.common.oss.minio

import org.springframework.boot.context.properties.ConfigurationProperties

/**
@author qins
@date 2023/8/11
@desc MINIO 配置项
 */
@ConfigurationProperties(prefix = "minio")
class MinioProperties {

    /**
     * 服务地址
     */
    var endpoint = ""

    /**
     * 访问key
     */
    var accessKey = ""

    /**
     * 访问secret
     */
    var secret = ""

    /**
     * 桶bucket
     */
    var bucket = ""

    /**
     * 存储路径
     */
    var path = "lowcode/upload/"

    /**
     * 文件访问前缀，如果配置，则直接拼接文件访问路径，否则通过API获取文件访问URL（该方式url最大有效期7天）
     */
    var fileUrlPrefix = ""
}