package com.terra.ns.imp.common.vo.ext

import com.terra.ns.imp.common.vo.response.PageResult

/**
@author qins
@date 2023/6/27
@desc 分页接口扩展操作
 */
/**
 * 判断分页结果是否为空
 */
fun <T> PageResult<T>.isEmpty() = totalCount <= 0 || result.isNullOrEmpty()

fun <T> PageResult<T>.isNotEmpty() = !isEmpty()

/**
 * 将分页结果对象的实体列表转化为目标实体对象
 * todo 将转化函数作为入参，统一在这里执行
 */
fun <T, R> PageResult<T>.convert(response: List<R>): PageResult<R> {
    return PageResult(this.pageNo, this.pageSize, this.totalCount, response)
}
