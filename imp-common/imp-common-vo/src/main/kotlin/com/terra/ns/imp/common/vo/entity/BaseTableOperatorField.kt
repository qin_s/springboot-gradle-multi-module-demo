package com.terra.ns.imp.common.vo.entity

import com.baomidou.mybatisplus.annotation.FieldFill
import com.baomidou.mybatisplus.annotation.TableField

/**
@author qins
@date 2023/5/31
@desc 数据库含有操作者（创建人和更新人）属性
 */
open class BaseTableOperatorField : BaseTableField() {
    /**
     * 创建人
     */
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    var createUser: String? = null

    /**
     * 修改人
     */
    @TableField(value = "modify_user", fill = FieldFill.INSERT_UPDATE)
    var modifyUser: String? = null

}