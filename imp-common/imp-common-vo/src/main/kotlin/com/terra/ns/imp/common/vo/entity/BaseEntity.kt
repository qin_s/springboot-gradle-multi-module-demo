package com.terra.ns.imp.common.vo.entity

import java.io.Serializable

/**
@author qins
@date 2023/6/9
 */
abstract class BaseEntity: Serializable