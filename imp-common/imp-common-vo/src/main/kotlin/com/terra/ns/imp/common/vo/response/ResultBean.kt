package com.terra.ns.imp.common.vo.response

import cn.hutool.core.util.StrUtil
import com.terra.ns.imp.common.core.constant.GlobalErrorCode.SUCCESS
import com.terra.ns.imp.common.core.exception.ErrorCode
import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/5/29
@desc WebMvc类接口统一返回类型
 */
//@Schema(description = "响应实体")
class ResultBean<T>(
    @Schema(description = "结果编码")
    var code: Int = 0,

    @Schema(description = "结果提示信息")
    var msg: String = "",

    @Schema(description = "结果数据")
    var data: T? = null,
) {
    constructor(error: ErrorCode, data: T?): this(error.code, error.msg, data)
    constructor(error: ErrorCode, msg: String, data: T?) : this(error.code, msg, data)
    constructor(code: Int, msg: String) : this() {
        this.code = code
        this.msg = msg
    }

    companion object {

        /**
         * 操作成功无数据返回
         */
        fun <T> success() : ResultBean<T> {
            return success(null)
        }

        /**
         * 操作成功有数据返回，数据可为null
         */
        fun <T> success(data: T?) : ResultBean<T> {
            return ResultBean(SUCCESS, data)
        }

//        /**
//         * 操作成交覆盖默认成功提示信息
//         */
//        fun <T> success(msg: String) : ResultBean<T> {
//            return success(null, msg)
//        }
        /**
         * 操作成交覆盖默认成功提示信息，含数据
         */
        fun <T> success(data: T?, msg: String) : ResultBean<T> {
            return ResultBean(SUCCESS, msg, data)
        }

        /**
         * 操作失败，要求只返回错误码
         */
        fun <T> fail(error: ErrorCode, vararg params: String) : ResultBean<T> {
            if(params.isNotEmpty()) {
                return ResultBean(error, StrUtil.format(error.msg, *params),null)
            }
            return ResultBean(error, null)
        }

        /**
         * 操作失败，要求只返回错误码
         */
        fun <T> fail(error: ErrorCode, errorMsg: String) : ResultBean<T> {
            return ResultBean(error, errorMsg.ifBlank { error.msg }, null)
        }

        /**
         * 操作失败，要求只返回错误码
         */
        fun <T> fail(code: Int, errorMsg: String) : ResultBean<T> {
            return ResultBean(code, errorMsg)
        }
    }

}