package com.terra.ns.imp.common.vo.request

import DATE_FORMAT_19
import com.terra.ns.imp.common.vo.validate.StringLocalDateTimeFormat
import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/7/12
@desc 根据某个时间字段范围查询
 */
@Schema(description = "某个时间字段范围")
open class QueryByDatetimeRequest (

    @Schema(description = "开始时间 yyyy-MM-dd HH:mm:ss")
    @get:StringLocalDateTimeFormat(allowNull = true, format = DATE_FORMAT_19, message = "时间格式错误")
    var dateTimeOpen: String? = null,

    @Schema(description = "结束时间, yyyy-MM-dd HH:mm:ss")
    @get:StringLocalDateTimeFormat(allowNull = true, format = DATE_FORMAT_19, message = "时间格式错误")
    var dateTimeClose: String? = null

)