package com.terra.ns.imp.common.vo.response

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/05/29
@desc 分页查询结果带汇总对象实体
 */
//@Schema(description = "分页数据实体-带汇总数据")
data class PageResultSummary<T, R>(

    @Schema(description = "汇总数据实体") var summaryResult: R?,

) : PageResult<T>()