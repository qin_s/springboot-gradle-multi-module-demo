package com.terra.ns.imp.common.vo.request

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/6/6
@desc 根据关键字查询
 */
@Schema(description = "根据关键字查询")
open class QueryByKeywordsRequest {

    @Schema(description = "关键字")
    var keywords : String? = null
}