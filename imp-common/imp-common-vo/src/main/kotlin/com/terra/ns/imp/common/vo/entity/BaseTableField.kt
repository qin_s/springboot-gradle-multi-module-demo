package com.terra.ns.imp.common.vo.entity

import com.baomidou.mybatisplus.annotation.FieldFill
import com.baomidou.mybatisplus.annotation.TableField
import java.io.Serializable
import java.time.LocalDateTime

/**
@author qins
@date 2023/5/31
@desc 数据库公共属性
 */
open class BaseTableField : Serializable {
    /**
     * 自增主键ID
     */
    var id: Long? = null

    /**
     * 逻辑删除0：正常数据，1：已删除
     */
    var deleted: Int = 0

    /**
     * 创建时间
     */
    @TableField(value = "create_date", fill = FieldFill.INSERT)
    var createDate: LocalDateTime? = null

    /**
     * 修改时间
     */
    @TableField(value = "modify_date", fill = FieldFill.INSERT_UPDATE)
    var modifyDate: LocalDateTime? = null
}