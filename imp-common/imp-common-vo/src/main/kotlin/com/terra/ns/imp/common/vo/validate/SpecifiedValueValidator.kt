package com.terra.ns.imp.common.vo.validate

import jakarta.validation.ConstraintValidator
import jakarta.validation.ConstraintValidatorContext

/**
@author qins
@date 2021/12/17
@desc
 */
class SpecifiedValueValidator: ConstraintValidator<SpecifiedValue, Any> {

    private var strValue: Array<String> = emptyArray()
    private var intValue: IntArray = intArrayOf()
    private var allowNull: Boolean = false

    override fun initialize(constraintAnnotation: SpecifiedValue) {
        strValue = constraintAnnotation.strValue
        intValue = constraintAnnotation.intValue
        allowNull = constraintAnnotation.allowNull
    }

    override fun isValid(value: Any?, context: ConstraintValidatorContext?): Boolean {
        if(value == null) {
            return allowNull
        }
        return when(value) {
            is String  -> {
                strValue.contains(value)
            }
            is Int -> {
                intValue.contains(value)
            }
            else -> {
                false
            }
        }
    }

}