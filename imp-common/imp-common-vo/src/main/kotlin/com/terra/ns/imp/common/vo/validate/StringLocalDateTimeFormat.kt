package com.terra.ns.imp.common.vo.validate

import jakarta.validation.Constraint
import jakarta.validation.Payload
import kotlin.reflect.KClass

/**
 * @author qins
 * @date 2022/5/25
 * @desc 校验入参String类型日期格式
 * @example @get:StringLocalDateTimeFormat(allowNull=true,format=DATE_FORMAT_16 message="时间格式错误")
 */
@Target(AnnotationTarget.PROPERTY, AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.PROPERTY_GETTER)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Constraint(validatedBy = [StringDateFormatValidator::class])
annotation class StringLocalDateTimeFormat(


    val message: String,

    /**
     * 是否允许为空
     */
    val allowNull: Boolean = false,

    /**
     * 格式，支持LocalDateFormatExtension.kt DATE_FORMAT_19,DATE_FORMAT_16,DATE_FORMAT_15, DATE_FORMAT_8,DATE_FORMAT_10
     */
    val format: String,

    val groups: Array<KClass<*>> = [],

    val payload: Array<KClass<out Payload>> = []
)

