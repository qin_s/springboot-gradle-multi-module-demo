package com.terra.ns.imp.common.vo.request

import io.swagger.v3.oas.annotations.media.Schema


/**
@author qins
@date 2023/5/25
@desc 根据Code集请求统一请求实体， code集可为空
 */
@Schema(description = "根据编码集请求体")
data class ByCodeListRequest(

    @Schema(description = "编码集合")
    var codeList: List<String>? = null
)