package com.terra.ns.imp.common.vo.request

import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank


/**
@author qins
@date 2023/5/25
@desc 根据Code请求统一请求实体， 要求Code不为空
 */
@Schema(description = "根据编码请求体")
data class ByCodeRequest(

    @get:NotBlank(message = "编码不能为空")
    @Schema(description = "请求编码")
    var code: String = ""
)