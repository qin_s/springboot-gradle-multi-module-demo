package com.terra.ns.imp.common.vo.response

import com.terra.ns.imp.common.vo.request.PageParam
import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/05/29
@desc 分页查询结果实体
 */
//@Schema(description = "分页数据实体")
open class PageResult<T>(
    @Schema(description = "当前页") var pageNo: Long = PageParam.PAGE_NO_DEFAULT,
    @Schema(description = "每页大小") var pageSize: Long  = PageParam.PAGE_SIZE_DEFAULT,
    @Schema(description = "数据总量") var totalCount: Long  = 0,
    @Schema(description = "分页数据") var result: List<T>? = null,
) {

    @Schema(description = "总页数")
    var totalPageCount: Long = 0

    /**
     *创建对象时自动计算总页数
     */
    init {
        this.totalPageCount = if(totalCount == 0L) 0 else (totalCount - 1) / pageSize + 1
    }
}