package com.terra.ns.imp.common.vo.validate

import jakarta.validation.Constraint
import jakarta.validation.Payload
import kotlin.reflect.KClass

/**
 * @author qins
 * @date 2021年12月17日
 * @desc 接口请求参数校验指定值的校验注解
 * @example @get:SpecifiedValue(intValue=[1,2],message="参数xx不在指定值中")
 */
@Target(AnnotationTarget.PROPERTY_GETTER)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Constraint(validatedBy = [SpecifiedValueValidator::class])
annotation class SpecifiedValue(
    val message: String,

    // 整型指定值
    val intValue: IntArray = [],

    // 字符串指定值
    val strValue: Array<String> = [],

    // 是否允许为空
    val allowNull: Boolean = false,

    val groups: Array<KClass<*>> = [],

    val payload: Array<KClass<out Payload>> = []
)

