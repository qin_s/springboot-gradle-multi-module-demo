package com.terra.ns.imp.common.vo.request

import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.Valid

/**
@author qins
@date 2023/05/29
@desc 分页请求参数
 */
@Schema(description = "分页参数")
open class PageParam<T> {

    @Schema(description = "当前页，默认1")
    var pageNo: Long = PAGE_NO_DEFAULT //当前页
    get() = if(field > 0) field else PAGE_NO_DEFAULT

    @Schema(description = "页大小，默认10")
    var pageSize: Long = PAGE_SIZE_DEFAULT //页大小
    get() = if(field > 0) field else PAGE_SIZE_DEFAULT

    @Schema(description = "请求参数体")
    @Valid
    var param : T? = null

    @get:Schema(description = "在分页条件下，分页开始位置，由分页参数自动计算", readOnly = true, hidden = true)
    val startNo: Long
    get() = (this.pageNo - 1) * this.pageSize

    companion object {

        const val PAGE_SIZE_DEFAULT = 10L

        const val PAGE_NO_DEFAULT = 1L
    }
}