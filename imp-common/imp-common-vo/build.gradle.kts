
dependencies {
    api(projectLibs.knife4j) {
        exclude("org.springdoc") // 排除默认版本
    }
    // 引入springdoc新版本
    api(projectLibs.bundles.spring.doc)
    api(project(":imp-common:imp-common-core"))
    api(project(":imp-common:imp-common-util"))
    api("com.baomidou:mybatis-plus-core:${projectLibs.versions.mybatis.plus.get()}")
}