package com.terra.ns.imp.common.core.exception

/**
@author qins
@date 2023/5/25
@desc 基础异常
 */
open class BaseException(var code: Int, msg: String) : RuntimeException(msg)

