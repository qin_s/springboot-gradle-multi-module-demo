package com.terra.ns.imp.common.core.definition

/**
@author qins
@date 2023/5/25
@desc 模块属性定义，每个模块类继承该类，且不可更改
 */
abstract class AbstractModuleDefinition(var code: String, var name: String, var desc: String): ModuleDefinition