package com.terra.ns.imp.common.core.constant

import com.terra.ns.imp.common.core.definition.ErrorCodeDefinition
import com.terra.ns.imp.common.core.exception.ErrorCode

/**
@author qins
@date 2023/5/25
@desc 全局错误码
 */
object GlobalErrorCode : ErrorCodeDefinition {

    //成功编码，全局统一为0
    val SUCCESS = ErrorCode(0, "操作成功")

    val REQUEST_PARAM_ERROR = ErrorCode(400, "请求参数不正确。{}")
    val USER_NO_LOGIN = ErrorCode(401, "登录信息无效，请重新登录")
    val USER_NO_PERM = ErrorCode(403, "没有操作权限")
    val REQUEST_NOT_FOUND = ErrorCode(404, "请求资源未找到。{}")
    val METHOD_NOT_ALLOWED = ErrorCode(405, "请求类型错误。{}");
    val REPEAT_REQUEST = ErrorCode(480, "请勿重复请求");
    val INTERNAL_SERVER_ERROR = ErrorCode(500, "系统错误")
    val REDISSON_CLIENT_GET_LOCK_FAIL = ErrorCode(900, "系统繁忙，请稍后再试")

}