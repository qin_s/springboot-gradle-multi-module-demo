package com.terra.ns.imp.common.core.constant

/**
@author qins
@date 2023/7/4
@desc 全局Bool类型的枚举
 */
enum class GlobalBoolEnum(val code: Int, val desc: String) {
    YES(1, "是"),
    NO(0, "否");

    companion object {
        fun getDesc(code: Int?): String? {
            code?.let {
                return values().firstOrNull { it.code == code }?.desc
            }
            return null
        }

        fun isTrue(code: Int?) : Boolean =
            code == YES.code
    }
}