package com.terra.ns.imp.common.core.exception

/**
@author qins
@date 2023/5/25
@desc 错误码
 */
class ErrorCode (val code: Int, val msg: String)