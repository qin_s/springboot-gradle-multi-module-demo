package com.terra.ns.imp.common.core.exception

import cn.hutool.core.util.StrUtil
import com.terra.ns.imp.common.core.definition.AbstractModuleDefinition

/**
@author qins
@date 2023/5/25
@desc 业务异常
 */
open class ServiceException(module: AbstractModuleDefinition, var errorCode: ErrorCode, vararg params: String) :
    BaseException(errorCode.code, "${module.code} 模块：${StrUtil.format(errorCode.msg, *params)}") {

       var shortMsg : String

       init {
           shortMsg = StrUtil.format(errorCode.msg, *params)
       }
    }