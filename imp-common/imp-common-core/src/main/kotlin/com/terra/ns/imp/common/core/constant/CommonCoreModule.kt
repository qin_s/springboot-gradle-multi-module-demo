package com.terra.ns.imp.common.core.constant

import com.terra.ns.imp.common.core.definition.AbstractModuleDefinition

object CommonCoreModule: AbstractModuleDefinition("imp-common-core", "公共定义", "公共定义")