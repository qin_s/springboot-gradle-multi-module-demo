package com.terra.ns.imp.common.core.definition

/**
@author qins
@date 2023/5/25
@desc 统一错误码定义
 * 错误码规则
 * 1. 全局通用错误码  3位长度
 * @example：
 * 401：未登录/登录信息失效
 * 403：未授权
 * ...
 * 999：未知错误
 * 2. 业务编码 8位
 * 0 : 1开头，其它预留
 * 1-2： 两位：模块位 所属模块(大级别模块如产品，交易，系统设置)...
 * 3-7： 自定义错误码-使用逐步自增方式，可用一到两位定义子模块
 * @example
 * 10100001: 项目代码已存在
 */
interface  ErrorCodeDefinition {

}