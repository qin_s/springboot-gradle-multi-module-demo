package com.terra.ns.imp.common.web.idempotent

import org.aspectj.lang.JoinPoint

/**
 * @author qins
 * @date 2022/04/14
 * @desc 定义幂等性的唯一Key解析器
 */
interface IdempotentKeyResolver {

    /**
     *@param joinPoint 切面
     * @return 得到解析后的唯一key
     */
    fun resolver(joinPoint: JoinPoint): String
}