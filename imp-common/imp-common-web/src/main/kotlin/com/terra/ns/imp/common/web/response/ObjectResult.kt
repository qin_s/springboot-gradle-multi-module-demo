package com.terra.ns.imp.common.web.response

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/5/29
@desc 构造统一返回结构 { code: 0, msg: "", data: {result: T, other:Any} }
 */
//@Schema(description = "非分页数据实体")
class ObjectResult<T>(@Schema(description = "结果数据") var result : T? = null)