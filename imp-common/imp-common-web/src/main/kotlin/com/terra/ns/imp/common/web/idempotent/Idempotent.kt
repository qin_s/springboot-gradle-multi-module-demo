package com.terra.ns.imp.common.web.idempotent

import java.util.concurrent.TimeUnit

/**
 * @author qins
 * @date 2022/4/13
 * @desc 接口要求做幂等的注解  主要用于防止写操作的重复提交
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class Idempotent(

    /**
     * 接口间隙时长
     * 如果接口执行时间超过了该时间，下一个请求还是会达到
     */
    val timeout: Long = 2,

    /**
     * 时长单位
     */
    val timeUnit: TimeUnit = TimeUnit.SECONDS,

)
