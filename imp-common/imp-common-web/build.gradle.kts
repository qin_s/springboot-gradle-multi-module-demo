dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-aop")
    api(project(":imp-common:imp-common-core"))
    api(project(":imp-common:imp-common-vo"))
    api(project(":imp-common:imp-common-util"))
    api(project(":imp-common:imp-common-redis"))
    api(project(":imp-common:imp-common-satoken"))
    api(projectLibs.satoken.web) // 引入satoken在webmvc环境的依赖
}