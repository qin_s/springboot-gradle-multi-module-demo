package com.terra.ns.imp.system.vo.request

import com.terra.ns.imp.system.vo.base.RoleBaseRequest
import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank

/**
@author qins
@date 2023/6/6
@desc 编辑角色信息请求体
 */
@Schema(description = "编辑角色实体")
data class EditRoleRequest(

    @Schema(description = "角色编码", required = true, example = "code1")
    @get:NotBlank(message = "角色编码不能为空")
    var code: String = "",

) : RoleBaseRequest()
