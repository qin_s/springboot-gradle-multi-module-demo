package com.terra.ns.imp.system.vo.base

import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Size

/**
@author qins
@date 2023/6/6
@desc 角色信息请求体
 */
@Schema(description = "角色基础信息")
open class RoleBaseRequest {

    @Schema(description = "角色名称", required = true, example = "部门管理员")
    @get:NotBlank(message = "角色名称不能为空")
    @get:Size(max=50, message = "角色名称长度不能超过50个字符")
    var name : String? = null

    @Schema(description = "备注", example = "备注信息")
    @get:Size(max = 200, message = "备注信息不能超过200个字符")
    var remark: String? = null
}