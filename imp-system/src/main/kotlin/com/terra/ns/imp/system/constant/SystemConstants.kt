package com.terra.ns.imp.system.constant

import com.terra.ns.imp.common.core.definition.ConstantDefinition
import java.util.concurrent.TimeUnit

/**
@author qins
@date 2023/6/5
@desc 系统模块常量定义
 */
object SystemConstants : ConstantDefinition {

    const val MODULE_CODE = "imp-SYSTEM" // 模块编码

    const val TOP_DEPT_PARENT_CODE = "0" // 顶级部门编码

    const val DING_TOP_DEPT_ID = 1L // 钉钉企业顶级部门ID

    const val LOCK_WAIT_TIME = 20L // 获取分布式锁等待时长
    const val LOCK_LEASE_TIME = 180L // 最多占据分布式锁时长
    val LOCK_TIME_UNIT = TimeUnit.SECONDS // 时长单位
}