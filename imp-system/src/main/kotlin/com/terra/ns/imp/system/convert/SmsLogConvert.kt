package com.terra.ns.imp.system.convert

import com.terra.ns.imp.system.entity.StSmsLog

/**
@author qins
@date 2023/6/8
@desc
 */
object SmsLogConvert {

    fun convert(
        uuid: String, phone: String, templateCode: String, content: String, param: String, status: Int, remark: String?
    ): StSmsLog {
        val smsLog = StSmsLog()
        smsLog.code = uuid
        smsLog.phone = phone
        smsLog.templateCode = templateCode
        smsLog.templateParam = param
        smsLog.sendContent = content
        smsLog.status = status
        smsLog.remark = remark
        return smsLog
    }
}