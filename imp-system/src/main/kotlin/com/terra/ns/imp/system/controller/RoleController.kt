package com.terra.ns.imp.system.controller

import com.terra.ns.imp.common.log.OperateLog
import com.terra.ns.imp.common.log.OperateTypeEnum
import com.terra.ns.imp.common.vo.request.ByCodeRequest
import com.terra.ns.imp.common.vo.request.PageParam
import com.terra.ns.imp.common.vo.request.QueryByKeywordsRequest
import com.terra.ns.imp.common.vo.response.PageResult
import com.terra.ns.imp.common.vo.response.ResultBean
import com.terra.ns.imp.common.web.idempotent.Idempotent
import com.terra.ns.imp.system.constant.SystemConstants.MODULE_CODE
import com.terra.ns.imp.system.service.IStRoleService
import com.terra.ns.imp.system.vo.base.RoleBaseRequest
import com.terra.ns.imp.system.vo.base.RoleBaseResponse
import com.terra.ns.imp.system.vo.request.EditRoleRequest
import com.terra.ns.imp.system.vo.request.RoleAssignPermRequest
import com.terra.ns.imp.system.vo.request.RoleAssignUserRequest
import com.terra.ns.imp.system.vo.response.RolePermResponse
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.validation.Valid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
@author qins
@date 2023/6/6
@desc 角色管理接口
 */
@RequestMapping("/role")
@RestController
@Tag(name = "role")
class RoleController {

    @Autowired
    private lateinit var roleService: IStRoleService

    @PostMapping("/addRole")
    @Operation(summary = "新增角色")
    @OperateLog(MODULE_CODE, OperateTypeEnum.ADD)
    @Idempotent
    fun addRole(@RequestBody @Valid request: RoleBaseRequest): ResultBean<String> {
        roleService.addRole(request)
        return ResultBean.success()
    }

    @PostMapping("/getRoleDetail")
    @Operation(summary = "查询角色详细", description = "根据角色编码")
    fun getRoleDetail(@RequestBody @Valid request: ByCodeRequest): ResultBean<RoleBaseResponse> {
        return ResultBean.success(roleService.selectRoleDetail(request))
    }

    @PostMapping("/getRoleList")
    @Operation(summary = "查询角色列表")
    fun getRoleList(@RequestBody @Valid request: QueryByKeywordsRequest): ResultBean<List<RoleBaseResponse>> {
        return ResultBean.success(roleService.selectRoleList(request))
    }

    @PostMapping("/getRolePage")
    @Operation(summary = "分页查询角色列表")
    fun getRolePage(@RequestBody @Valid request: PageParam<QueryByKeywordsRequest>): ResultBean<PageResult<RoleBaseResponse>> {
        return ResultBean.success(roleService.selectRolePage(request))
    }

    @PostMapping("/editRole")
    @Operation(summary = "编辑角色")
    @OperateLog(MODULE_CODE, OperateTypeEnum.MODIFY)
    @Idempotent
    fun editRole(@RequestBody @Valid request: EditRoleRequest): ResultBean<Any> {
        roleService.updateRole(request)
        return ResultBean.success()
    }

    @PostMapping("/deleteRole")
    @Operation(summary = "删除角色", description = "根据角色编码")
    @OperateLog(MODULE_CODE, OperateTypeEnum.DELETE)
    @Idempotent
    fun deleteRole(@RequestBody @Valid request: ByCodeRequest): ResultBean<Any> {
        roleService.deleteRole(request)
        return ResultBean.success()
    }

    @PostMapping("/getRolePerm")
    @Operation(summary = "查询角色权限", description = "根据角色编码")
    fun getRolePerm(@RequestBody @Valid request: ByCodeRequest): ResultBean<RolePermResponse> {
        return ResultBean.success(roleService.selectRolePerm(request))
    }

    @PostMapping("/assignUser")
    @Operation(summary = "关联用户")
    @OperateLog(MODULE_CODE, OperateTypeEnum.MODIFY, "角色关联用户")
    @Idempotent
    fun assignUser(@RequestBody @Valid request: RoleAssignUserRequest): ResultBean<Any> {
        roleService.assignUser(request)
        return ResultBean.success()
    }

    @PostMapping("/assignPerm")
    @Operation(summary = "分配权限")
    @OperateLog(MODULE_CODE, OperateTypeEnum.MODIFY, "角色分配权限")
    @Idempotent
    fun assignPerm(@RequestBody @Valid request: RoleAssignPermRequest): ResultBean<Any> {
        roleService.assignPerm(request)
        return ResultBean.success()
    }
}