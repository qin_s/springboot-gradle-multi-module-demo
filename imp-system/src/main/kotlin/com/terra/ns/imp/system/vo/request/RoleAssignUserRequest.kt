package com.terra.ns.imp.system.vo.request

import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank

/**
@author qins
@date 2023/6/14
@desc 角色关联用户求实体
 */
@Schema(description = "角色关联用户求实体")
data class RoleAssignUserRequest (

    @Schema(description="角色编码", required = true, example = "code")
    @get:NotBlank(message = "角色编码不能为空")
    var code : String = "",

    @Schema(description = "用户编码列表")
    var userCodeList: List<String>? = null
)