package com.terra.micro.portal.common.service.vo.organization.user.request

import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank

/**
@author qins
@date 2022/4/13
@desc 编辑密码请求实体
 */
@Schema(description = "修改密码请求实体")
data class EditPasswordRequest(

    @Schema(description = "原密码")
    @get:NotBlank(message = "原密码不能为空")
    var oldPassword: String? = null,

    @Schema(description = "新密码")
    @get:NotBlank(message = "新密码不能为空")
    var password: String? = null,
)