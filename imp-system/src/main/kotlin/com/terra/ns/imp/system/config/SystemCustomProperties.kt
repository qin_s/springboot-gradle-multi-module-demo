package com.terra.ns.imp.system.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

/**
@author qins
@date 2023/6/14
@desc 系统自定义配置属性
 */
@Configuration
@ConfigurationProperties(prefix = "lp.system")
class SystemCustomProperties {

    /**
     * 初始化登录密码
     */
    var initLoginPassword = "Ab123!@#"

    /**
     * 钉钉事件验证测试部门名称
     */
    var dingEventVerifyDeptName = "Terra低代码"
}