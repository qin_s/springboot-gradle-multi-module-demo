package com.terra.ns.imp.system.mapper

import com.terra.ns.imp.common.mybatis.mapper.BaseMapperX
import com.terra.ns.imp.system.entity.StSmsLog

/**
@author qins
@date 2023/6/7
@desc
 */
interface StSmsLogMapper: BaseMapperX<StSmsLog> {
}