package com.terra.ns.imp.system.vo.response

import com.terra.ns.imp.system.vo.base.PermBaseResponse
import com.terra.ns.imp.system.vo.base.RoleBaseResponse
import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/6/14
@desc 角色权限信息响应实体
 */
@Schema(description = "角色权限信息响应实体")
data class RolePermResponse (

    @Schema(description = "权限列表")
    var permList : List<PermBaseResponse>? = null,

    ) : RoleBaseResponse()