package com.terra.ns.imp.system.vo.response

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/8/10
@desc
 */
@Schema(description = "minio获取文件上传链接响应")
data class MinioFileUploadResponse(

    @Schema(description = "文件上传的url", required = true)
    var uploadUrl: String = "",

    @Schema(description = "上传到服务的唯一文件名称", required = true)
    var fileName: String = ""

)