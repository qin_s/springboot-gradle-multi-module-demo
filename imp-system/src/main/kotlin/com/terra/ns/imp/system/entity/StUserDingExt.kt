package com.terra.ns.imp.system.entity

import com.baomidou.mybatisplus.annotation.TableName
import com.terra.ns.imp.common.vo.entity.BaseTableField
import java.time.LocalDateTime

/**
 * <p>
 * 钉钉用户扩展属性表
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
@TableName("st_user_ding_ext")
class StUserDingExt : BaseTableField() {


    /**
     * 用户ID
     */
    var userid: String? = null

    /**
     * 用户编码
     */
    var userCode: String? = null

    /**
     * 姓名
     */
    var name: String? = null

    /**
     * 头像
     */
    var avatar: String? = null

    /**
     * 用户在当前开发者企业账号范围内的唯一标识
     */
    var unionId: String? = null

    /**
     * 国际电话区号
     */
    var stateCode: String? = null

    /**
     * 手机号码
     */
    var mobile: String? = null

    /**
     * 是否隐藏手机号（0：否；1：是）
     */
    var hideMobile: Int? = null

    /**
     * 员工工号
     */
    var jobNumber: String? = null

    /**
     * 职位
     */
    var title: String? = null

    /**
     * 员工邮箱
     */
    var email: String? = null

    /**
     * 员工的企业邮箱
     */
    var orgEmail: String? = null

    /**
     * 办公地点
     */
    var workPlace: String? = null

    /**
     * 备注
     */
    var remark: String? = null

    /**
     * 员工在部门中的排序
     */
    var deptOrder: Long? = null

    /**
     * 扩展属性
     */
    var extension: String? = null

    /**
     * 入职时间
     */
    var hiredDate: LocalDateTime? = null

    /**
     * 是否激活了钉钉：（0：否；1：是）
     */
    var active: Int? = null

    /**
     * 是否为企业的管理员：（0：否；1：是）
     */
    var admin: Int? = null

    /**
     * 是否为企业的老板：（0：否；1：是）
     */
    var boss: Int? = null

    /**
     * 是否是部门的主管：（0：否；1：是）
     */
    var leader: Int? = null

    /**
     * 是否专属帐号：（0：否；1：是）
     */
    var exclusiveAccount: Int? = null

    /**
     * 专属帐号登录名
     */
    var loginId: String? = null

    /**
     * 专属帐号类型：sso：企业自建专属帐号;.dingtalk：钉钉自建专属帐号
     */
    var exclusiveAccountType: String? = null

    /**
     * 查询本组织专属帐号时可获得昵称
     */
    var nickname: String? = null

    /**
     * 职位
     */
    var position: String? = null

}
