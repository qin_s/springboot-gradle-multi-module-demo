package com.terra.ns.imp.system.constant.enums

/**
@author qins
@date 2023/6/16
@desc 用户事件类型
 */
enum class UserEventEnum(val code: Int, val desc: String) {

    DELETE(0, "删除用户"),
}
