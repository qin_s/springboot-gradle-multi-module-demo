package com.terra.ns.imp.system.controller

import cn.hutool.core.util.IdUtil
import com.terra.ns.imp.common.oss.minio.MinioUtils
import com.terra.ns.imp.common.vo.response.ResultBean
import com.terra.ns.imp.common.web.idempotent.Idempotent
import com.terra.ns.imp.system.vo.request.GetMinioFileUploadRequest
import com.terra.ns.imp.system.vo.response.MinioFileUploadResponse
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


/**
@author qins
@date 2023/8/10
@desc 文件处理
 */
@RequestMapping("/file")
@RestController
@Tag(name = "file")
class FileController {

    @Autowired
    private lateinit var minioUtils: MinioUtils

    @PostMapping("/getFileTempUploadUrl")
    @Operation(summary = "获取文件上传临时URL", description = "minio需要")
    @Idempotent
    fun getFileTempUploadUrl(@RequestBody  @Validated request: GetMinioFileUploadRequest): ResultBean<MinioFileUploadResponse> {
        val fileName = IdUtil.getSnowflakeNextIdStr() + "." + request.fileName.split(".")[1]
        return ResultBean.success(MinioFileUploadResponse(minioUtils.getUploadUrl(fileName), fileName))
    }

    @PostMapping("/getFileUrl")
    @Operation(summary = "获取文件访问URL")
    fun getFileUrl(@RequestBody @Validated request: GetMinioFileUploadRequest): ResultBean<String> {
        return ResultBean.success(minioUtils.getFileAccessUrl(request.fileName))
    }


    @PostMapping("/getAccessToken")
    @Operation(summary = "获取临时访问token", description = "阿里云oss需要")
    @Idempotent
    fun getAccessToken() {

    }
}