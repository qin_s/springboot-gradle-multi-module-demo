package com.terra.ns.imp.system.constant.enums

/**
@author qins
@date 2023/8/2
@desc 组织架构检索类型
 */
enum class OrgCheckTypeEnum(val type: String, val desc: String) {

    USER("user", "用户"),
    DEPT("department", "部门"),
    ROLE("role", "角色"),
}
