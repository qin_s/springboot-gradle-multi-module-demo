package com.terra.ns.imp.system.api

import cn.hutool.core.collection.CollUtil
import com.terra.ns.imp.system.api.entity.BaseRoleUser
import com.terra.ns.imp.system.api.service.IRoleApi
import com.terra.ns.imp.system.mapper.StUserRoleRelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
@author qins
@date 2023/7/10
@desc 角色api实现
 */
@Service
class RoleApiImpl : IRoleApi {

    @Autowired
    private lateinit var userRoleRelMapper: StUserRoleRelMapper

    override fun getAllUserByRoles(roleCodes: List<String>): List<BaseRoleUser> {
        val roleRelList = userRoleRelMapper.selectListByRoleCodes(roleCodes)
        if(roleRelList.isEmpty()) return CollUtil.newArrayList()
        return roleRelList.map { BaseRoleUser(it.roleCode!!, it.userCode!!) }
    }
}