package com.terra.ns.imp.system.vo.response.ding

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/6/6
@desc 绑定钉钉响应
 */
@Schema(description = "绑定钉钉响应实体")
data  class DdStructureBindResponse(

    @Schema(description = "是否通过")
    var result: Boolean = true,

    @Schema(description ="企业全称")
    var nameCn: String? = "",

    @Schema(description ="企业简称")
    var nameCnShort: String? = "",

    @Schema(description = "钉钉企业名称")
    var dingName: String? = "",
)