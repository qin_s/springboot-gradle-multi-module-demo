package com.terra.ns.imp.system.vo.request

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/6/6
@desc 根据code查询部门树
 */
@Schema(description = "根据code查询部门树")
data class QueryDeptTreeRequest(

    @Schema(description = "部门编码code，如果为空则则查询整颗树")
    var code: String? = null,

    @Schema(description = "部门名称, 过滤")
    var name: String? = null
)