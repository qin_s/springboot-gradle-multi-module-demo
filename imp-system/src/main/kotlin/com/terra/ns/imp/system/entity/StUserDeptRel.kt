package com.terra.ns.imp.system.entity

import com.baomidou.mybatisplus.annotation.TableName
import com.terra.ns.imp.common.vo.entity.BaseTableField

/**
 * <p>
 * 用户部门关系表
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
@TableName("st_user_dept_rel")
class StUserDeptRel : BaseTableField() {

    /**
     * 用户编码
     */
    var userCode: String? = null

    /**
     * 机构编码
     */
    var departmentCode: String? = null

    /**
     * 是否主部门（1：主部门；0：非主部门）
     */
    var mainDept: Int? = null

}
