package com.terra.ns.imp.system.mapper

import com.terra.ns.imp.common.mybatis.mapper.BaseMapperX
import com.terra.ns.imp.system.entity.StSmsTemplate

/**
@author qins
@date 2023/6/7
@desc
 */
interface StSmsTemplateMapper: BaseMapperX<StSmsTemplate> {
    fun selectOneByType(type: Int): StSmsTemplate?
        = selectOneByField(StSmsTemplate::class.java, StSmsTemplate::type, type)
}