package com.terra.ns.imp.system.convert

import com.terra.ns.imp.system.entity.StUserRoleRel


/**
@author qins
@date 2023/6/14
@desc
 */
object UserRoleRelConvert {

    fun convert(userCode: String, roleCode: String): StUserRoleRel {
        val stUserRoleRel = StUserRoleRel()
        stUserRoleRel.userCode = userCode
        stUserRoleRel.roleCode = roleCode
        return stUserRoleRel
    }
}