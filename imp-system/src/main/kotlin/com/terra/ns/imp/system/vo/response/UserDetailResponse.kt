package com.terra.ns.imp.system.vo.response

import com.terra.ns.imp.system.vo.base.DeptBaseResponse
import com.terra.ns.imp.system.vo.base.RoleBaseResponse
import com.terra.ns.imp.system.vo.base.UserBaseResponse
import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/6/14
@desc 用户详细响应体
 */
@Schema(description = "用户详细响应体")
data class UserDetailResponse (

//    @Schema(description = "主部门信息")
//    var mainDept : DeptBaseResponse? = null,

    @Schema(description = "部门信息")
    var deptList : List<DeptBaseResponse>? = null,

    @Schema(description = "角色信息")
    var roleList : List<RoleBaseResponse>? = null

) : UserBaseResponse()