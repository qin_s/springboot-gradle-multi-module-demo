package com.terra.ns.imp.system.vo.request

import com.terra.ns.imp.common.vo.request.QueryByKeywordsRequest
import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/8/2
@desc 检索企业部门-人员-角色请求
 */
@Schema(description = "检索企业部门-人员-角色请求")
data class QueryOrgRequest(

    @Schema(description = "查询类型 role|department|user")
    var type: List<String>? = null,

    @Schema(description = "取下级的部门编码")
    var key: String? = null

) : QueryByKeywordsRequest()