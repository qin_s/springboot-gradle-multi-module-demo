package com.terra.ns.imp.system.convert

import com.terra.ns.imp.common.dingtalk.contact.domain.UserDetail
import com.terra.ns.imp.common.dingtalk.contact.domain.UserV2DTO
import com.terra.ns.imp.system.entity.StUserDingExt
import millsToLocalDateTime
import toLocalDateTime

object UserDingExtConvert {

    fun convert(userV2DTO: UserV2DTO): StUserDingExt {
        val stUserDingExt = StUserDingExt()
        stUserDingExt.unionId = userV2DTO.unionid
        stUserDingExt.extension = userV2DTO.extattr
        stUserDingExt.hiredDate = userV2DTO.hiredDate.toLocalDateTime()
        stUserDingExt.userid = userV2DTO.userid
        stUserDingExt.name = userV2DTO.name
        stUserDingExt.avatar = userV2DTO.avatar
        stUserDingExt.stateCode = userV2DTO.stateCode
        stUserDingExt.mobile = userV2DTO.mobile
        stUserDingExt.jobNumber = userV2DTO.jobNumber
        stUserDingExt.title = userV2DTO.title
        stUserDingExt.email = userV2DTO.email
        stUserDingExt.orgEmail = userV2DTO.orgEmail
        stUserDingExt.workPlace = userV2DTO.workPlace
        stUserDingExt.position = userV2DTO.position
        stUserDingExt.remark = userV2DTO.remark
        stUserDingExt.deptOrder = userV2DTO.order
        stUserDingExt.active = if (userV2DTO.active == null || !userV2DTO.active) 0 else 1
        stUserDingExt.admin = if (userV2DTO.admin == null || !userV2DTO.admin) 0 else 1
        stUserDingExt.boss = if (userV2DTO.boss == null || !userV2DTO.boss) 0 else 1
        stUserDingExt.leader = if (userV2DTO.leader == null || !userV2DTO.leader) 0 else 1
        stUserDingExt.hideMobile = if (userV2DTO.hide == null || !userV2DTO.hide) 0 else 1
        return stUserDingExt
    }

    fun convert(dingUser: UserDetail): StUserDingExt {
        val stUserDingExt = StUserDingExt()
        stUserDingExt.unionId = dingUser.unionid
        dingUser.hiredDate?.let {
            stUserDingExt.hiredDate = dingUser.hiredDate.millsToLocalDateTime()
        }
        stUserDingExt.userid = dingUser.userid
        stUserDingExt.name = dingUser.name
        stUserDingExt.avatar = dingUser.avatar
        stUserDingExt.stateCode = dingUser.stateCode
        stUserDingExt.mobile = dingUser.mobile
        stUserDingExt.jobNumber = dingUser.jobNumber
        stUserDingExt.title = dingUser.title
        stUserDingExt.email = dingUser.email
        stUserDingExt.orgEmail = dingUser.orgEmail
        stUserDingExt.workPlace = dingUser.workPlace
        stUserDingExt.remark = dingUser.remark
        stUserDingExt.loginId = dingUser.loginId
        stUserDingExt.hideMobile = if (dingUser.hideMobile == null || !dingUser.hideMobile) 0 else 1
        stUserDingExt.active = if (dingUser.active == null || !dingUser.active) 0 else 1
        stUserDingExt.admin = if (dingUser.admin == null || !dingUser.admin) 0 else 1
        stUserDingExt.boss = if (dingUser.boss == null || !dingUser.boss) 0 else 1
        stUserDingExt.exclusiveAccount = if (dingUser.exclusiveAccount == null || !dingUser.exclusiveAccount) 0 else 1
        //辨别是否是部门主管
        stUserDingExt.leader = if(dingUser.leaderInDept.firstOrNull { it.leader } == null) 0 else 1
        return stUserDingExt
    }
}