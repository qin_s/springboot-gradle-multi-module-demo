package com.terra.ns.imp.system.service

import com.terra.ns.imp.system.entity.StSmsLog

/**
@author qins
@date 2023/6/7
@desc
 */
interface IStSmsLogService {

    fun syncSaveLog(smsLog: StSmsLog)
}