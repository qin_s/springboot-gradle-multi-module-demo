package com.terra.ns.imp.system.vo.response.ding

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/6/16
@desc 绑定钉钉响应
 */
@Schema(description = "钉钉组织架构绑定信息响应实体")
data class DdStructureBindInfoResponse (

    @Schema(description = "同步策略（0:全量同步 1：增量同步）默认为全量")
    var syncType: Int? = null,

    @Schema(description = "钉钉事件接收aesKey")
    var eventAesKey: String? = null,

    @Schema(description = "钉钉事件接收token")
    var eventToken: String? = null,

    @Schema(description ="应用的唯一标识key")
    var appKey: String? = null,

    @Schema(description = "应用的密钥")
    var appSecret: String? = null,

    @Schema(description = "企业钉钉机构ID")
    var corpId: String? = null,

    @Schema(description = "代理ID")
    var agentId: String? = null,

    @Schema(description = "APP调用基础URL")
    var basicApiUrl: String? = null,

    @Schema(description = "同步策略描述")
    var syncTypeName: String? = null,

    @Schema(description = "是否实时更新策略描述")
    var immediateName: String? = null,

    @Schema(description = "事件参数校验状态 0：未验证 1：已验证")
    var eventVerifyStatus: Int? = null,

    @Schema(description = "事件参数校验状态描述")
    var eventVerifyStatusName: String? = null,

    @Schema(description = "创建时间")
    var createDate: String? = null,
)