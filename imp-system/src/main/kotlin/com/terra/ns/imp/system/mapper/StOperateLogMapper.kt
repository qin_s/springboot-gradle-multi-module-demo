package com.terra.ns.imp.system.mapper

import com.terra.ns.imp.common.mybatis.mapper.BaseMapperX
import com.terra.ns.imp.system.api.entity.StOperateLog

/**
 * <p>
 * 用户操作日志记录表 Mapper 接口
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
interface StOperateLogMapper : BaseMapperX<StOperateLog>
