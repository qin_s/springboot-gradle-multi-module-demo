package com.terra.ns.imp.system.entity

import com.baomidou.mybatisplus.annotation.TableName
import com.terra.ns.imp.common.vo.entity.BaseTableField

/**
 * <p>
 * 短信日志表
 * </p>
 * @author qins
 * @since 2023/6/7
 */
@TableName("st_sms_log")
class StSmsLog : BaseTableField() {

    /**
     * 短信唯一标识
     */
    var code: String? = null

    /**
     * 模板编码，如果有
     */
    var templateCode: String? = null
    /**
     * 模板参数，如果有
     */
    var templateParam: String? = null
    /**
     * 手机号
     */
    var phone: String? = null
    /**
     * 短信内容
     */
    var sendContent: String? = null

    /**
     * 数据状态0：发送成功，other: 其它错误状态码
     */
    var status: Int? = null

    /**
     * 备注信息
     */
    var remark: String? = null
}
