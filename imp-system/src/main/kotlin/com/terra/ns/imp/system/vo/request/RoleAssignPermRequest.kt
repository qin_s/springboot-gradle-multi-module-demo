package com.terra.ns.imp.system.vo.request

import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank

/**
@author qins
@date 2023/6/14
@desc 为角色分配权限求实体
 */
@Schema(description = "为角色分配权限求实体")
data class RoleAssignPermRequest (

    @Schema(description ="角色编码", required = true, example = "code")
    @get:NotBlank(message = "角色编码不能为空")
    var code : String = "",

    @Schema(description = "权限编码列表")
    var permCodeList: List<String>? = null
)