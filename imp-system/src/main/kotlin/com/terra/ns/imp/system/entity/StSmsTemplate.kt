package com.terra.ns.imp.system.entity

import com.baomidou.mybatisplus.annotation.TableName
import com.terra.ns.imp.common.vo.entity.BaseTableOperatorField

/**
 * @author qins
 * @since 2023/6/7
 * @desc 短信模板
 */
@TableName("st_sms_template")
class StSmsTemplate : BaseTableOperatorField() {

    /**
     * 模板类型
     */
    var type: String? = null
    /**
     * 模板名称
     */
    var name: String? = null
    /**
     * 模板编码
     */
    var templateCode: String? = null
    /**
     * 模板内容
     */
    var templateContent: String? = null
    /**
     * 短信签名
     */
    var signature: String? = null
    /**
     * 模板参数,如：${code},#{code} 多个以逗号分隔
     */
    var templateParam: String? = null

}
