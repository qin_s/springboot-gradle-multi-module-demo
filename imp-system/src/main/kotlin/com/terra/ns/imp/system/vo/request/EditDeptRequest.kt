package com.terra.ns.imp.system.vo.request

import com.terra.ns.imp.system.vo.base.DeptBaseRequest
import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank

/**
@author qins
@date 2023/6/6
@desc 编辑部门请求
 */
@Schema(description = "编辑部门实体")
data class EditDeptRequest (

    @Schema(description = "部门编码", required = true)
    @get:NotBlank(message = "部门编码不能为空")
    var code: String = "",

) : DeptBaseRequest()