package com.terra.ns.imp.system.mapper

import com.terra.ns.imp.common.mybatis.mapper.BaseMapperX
import com.terra.ns.imp.common.mybatis.mapper.KtQueryWrapperX
import com.terra.ns.imp.system.entity.StUserDeptRel

/**
 * <p>
 * 用户部门关系表 Mapper 接口
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
interface StUserDeptRelMapper : BaseMapperX<StUserDeptRel> {

    fun selectListByDeptCode(deptCode: String): List<StUserDeptRel>
            = selectList(KtQueryWrapperX(StUserDeptRel::class.java).eqX(StUserDeptRel::departmentCode, deptCode))

    fun selectListByUserCode(userCode: String) : List<StUserDeptRel>
        = selectList(KtQueryWrapperX(StUserDeptRel::class.java).eqX(StUserDeptRel::userCode, userCode))

    fun deleteByUserCode(userCode: String)
        = delete(KtQueryWrapperX(StUserDeptRel::class.java).eqX(StUserDeptRel::userCode, userCode))

    fun deleteByDeptCodes(delDeptCodeList: List<String>) {
        if(delDeptCodeList.isEmpty()) return
        delete(KtQueryWrapperX(StUserDeptRel::class.java).inX(StUserDeptRel::departmentCode, delDeptCodeList))
    }
    fun deleteByDeptCode(delDeptCode: String)
        = delete(KtQueryWrapperX(StUserDeptRel::class.java).eqX(StUserDeptRel::departmentCode, delDeptCode))

    fun deleteByUserCodes(delUserCodeList: List<String>) {
        if(delUserCodeList.isEmpty()) return
        delete(KtQueryWrapperX(StUserDeptRel::class.java).inX(StUserDeptRel::userCode, delUserCodeList))
    }

    fun deleteByUserCodesAndDeptCode(delUserCodeList: List<String>, deptCode: String) {
        if(delUserCodeList.isEmpty()) return
        delete(KtQueryWrapperX(StUserDeptRel::class.java).eqX(StUserDeptRel::departmentCode, deptCode).inX(StUserDeptRel::userCode, delUserCodeList))
    }

    fun selectListByUserCodes(userCodes: List<String>): List<StUserDeptRel> {
        if(userCodes.isEmpty()) return emptyList()
        return selectList(KtQueryWrapperX(StUserDeptRel::class.java).inX(StUserDeptRel::userCode, userCodes))
    }

    fun selectListByDeptCodes(deptCodes: List<String>): List<StUserDeptRel> {
        if(deptCodes.isEmpty()) return emptyList()
        return selectList(KtQueryWrapperX(StUserDeptRel::class.java).inX(StUserDeptRel::departmentCode, deptCodes))
    }

    fun selectOneByUserCodeAndDeptCode(userCode: String, stDeptCode: String): StUserDeptRel?
        = selectOne(KtQueryWrapperX(StUserDeptRel::class.java).eqX(StUserDeptRel::userCode, userCode).eqX(StUserDeptRel::departmentCode, stDeptCode).lastLimit1())


}
