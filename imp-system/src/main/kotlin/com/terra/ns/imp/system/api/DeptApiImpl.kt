package com.terra.ns.imp.system.api

import cn.hutool.core.collection.CollUtil
import com.terra.ns.imp.system.api.entity.BaseDeptUser
import com.terra.ns.imp.system.api.service.IDeptApi
import com.terra.ns.imp.system.mapper.StDepartmentMapper
import com.terra.ns.imp.system.mapper.StUserDeptRelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
@author qins
@date 2023/7/10
@desc 部门api实现
 */
@Service
class DeptApiImpl : IDeptApi {

    @Autowired
    private lateinit var userDeptRelMapper: StUserDeptRelMapper

    @Autowired
    private lateinit var departmentMapper: StDepartmentMapper

    override fun getAllUserByDepts(deptCodes: List<String>): List<BaseDeptUser> {
        val deptRelList = userDeptRelMapper.selectListByDeptCodes(deptCodes)
        if(deptRelList.isEmpty()) return CollUtil.newArrayList()
        return deptRelList.map { BaseDeptUser(it.departmentCode!!, it.userCode!!) }
    }

    override fun getDeptLeaderByDepts(deptCodes: List<String>): List<BaseDeptUser> {
        val hasLeaderList = departmentMapper.selectListByCodes(deptCodes)
            .filter { !it.principalCode.isNullOrBlank() }
        val result = ArrayList<BaseDeptUser>()
        hasLeaderList.forEach {
            result.addAll(it.principalCode!!.split(",").map { item -> BaseDeptUser(it.code!!, item) })
        }
        return result
    }
}