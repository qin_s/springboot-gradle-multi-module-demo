package com.terra.ns.imp.system.service.impl

import com.terra.ns.imp.system.entity.StSmsLog
import com.terra.ns.imp.system.mapper.StSmsLogMapper
import com.terra.ns.imp.system.service.IStSmsLogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service

/**
@author qins
@date 2023/6/7
@desc
 */
@Service
class StSmsLogServiceImpl: IStSmsLogService {

    @Autowired
    private lateinit var smsLogMapper: StSmsLogMapper

    @Async
    override fun syncSaveLog(smsLog: StSmsLog) {
        smsLogMapper.insert(smsLog)
    }
}