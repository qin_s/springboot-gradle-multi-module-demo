package com.terra.ns.imp.system.mapper

import com.baomidou.mybatisplus.extension.kotlin.KtQueryWrapper
import com.terra.ns.imp.common.mybatis.mapper.BaseMapperX
import com.terra.ns.imp.common.mybatis.mapper.KtQueryWrapperX
import com.terra.ns.imp.system.entity.StDepartment

/**
 * <p>
 * 企业部门表 Mapper 接口
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
interface StDepartmentMapper : BaseMapperX<StDepartment> {

    fun selectAllDept() : List<StDepartment>
        = selectList(KtQueryWrapper(StDepartment::class.java))

    fun selectByCode(parentCode: String): StDepartment?
        = selectOneByField(StDepartment::class.java, StDepartment::code, parentCode)

    fun selectByName(name: String): List<StDepartment>
        = selectList(
            KtQueryWrapperX(StDepartment::class.java)
                .eqX(StDepartment::name, name)
        )

    fun selectByLikeNodePath(nodePath: String): List<StDepartment>
        = selectList(KtQueryWrapperX(StDepartment::class.java).likeIfPresent(StDepartment::nodePath, nodePath))

    fun batchUpdateById(updateList: List<StDepartment>) {
        if(updateList.isEmpty()) return
        updateList.forEach { updateById(it) }
    }

    fun updateByCode(updateDept: StDepartment, deptCode: String) {
        updateByField(updateDept, StDepartment::code, deptCode)
    }

    fun selectByParentCode(parentCode: String): List<StDepartment>
        = selectList( KtQueryWrapperX(StDepartment::class.java)
            .eqX(StDepartment::parentCode, parentCode)
        )

    fun deleteByCode(deptCode: String)
        = deleteByField(StDepartment::class.java, StDepartment::code, deptCode)

    fun selectByLikeName(name: String?) : List<StDepartment>
        = selectList( KtQueryWrapperX(StDepartment::class.java)
            .likeIfPresent(StDepartment::name, name)
            )

    fun selectAllDept(tenantId: Long): List<StDepartment>
        = selectList(KtQueryWrapper(StDepartment::class.java))

    fun selectOneByCode(deptCode: String): StDepartment?
        = selectOne(
             KtQueryWrapperX(StDepartment::class.java).eqX(StDepartment::code, deptCode)
        )

    fun selectListByCodes(codes: List<String>): List<StDepartment> {
        if(codes.isEmpty()) return emptyList()
        return selectList(KtQueryWrapperX(StDepartment::class.java).inX(StDepartment::code, codes))
    }

    fun deleteByCodes(delDeptCodeList: List<String>) {
        if(delDeptCodeList.isEmpty()) return
        delete(KtQueryWrapperX(StDepartment::class.java).inX(StDepartment::code, delDeptCodeList))
    }

    fun updateAllFieldByCode(updateDept: StDepartment, code: String)
        = updateFieldIncludeNull(updateDept, KtQueryWrapperX(StDepartment::class.java).eqX(StDepartment::code, code), listOf(StDepartment::code.name))

    fun selectListByPrincipalCode(userCode: String): List<StDepartment>
        = selectList(KtQueryWrapperX(StDepartment::class.java).eqX(StDepartment::principalCode, userCode))

}
