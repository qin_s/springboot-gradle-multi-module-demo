package com.terra.ns.imp.system.vo.response

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/8/2
@desc
 */
@Schema(description = "检索企业部门-人员-角色条目信息")
data class OrgItemResponse(

    @Schema(description = "名称")
    var title: String? = null,

    @Schema(description = "唯一编码")
    var key: String? = null,

    @Schema(description = "类型 role|department|user")
    var type: String? = null,

    @Schema(description = "职位")
    var job: String? = null,

    @Schema(description = "描述")
    var desc: String? = null,

    @Schema(description = "是否有下级")
    var hasNext: Boolean = false,
)