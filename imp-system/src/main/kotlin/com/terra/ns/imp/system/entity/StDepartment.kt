package com.terra.ns.imp.system.entity

import com.baomidou.mybatisplus.annotation.TableName
import com.terra.ns.imp.common.vo.entity.BaseTableOperatorField

/**
 * <p>
 * 企业部门表
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
@TableName("st_department")
class StDepartment : BaseTableOperatorField() {

    /**
     * 部门唯一编码，code生成规则6位
     */
    var code: String? = null

    /**
     * 父结点编码
     */
    var parentCode: String? = null

    /**
     * 根节点编码
     */
    var topNodeCode: String? = null

    /**
     * 结点路径（以父到子顺序存放code路径，例：0007-0014-0026）
     */
    var nodePath: String? = null

    /**
     * 部门名称
     */
    var name: String? = null

    /**
     * 负责人编码
     */
    var principalCode: String? = null
}
