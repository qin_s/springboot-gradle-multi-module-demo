package com.terra.ns.imp.system.convert

import cn.hutool.core.util.IdUtil
import com.terra.ns.imp.common.dingtalk.contact.domain.UserDetail
import com.terra.ns.imp.common.dingtalk.contact.domain.UserV2DTO
import com.terra.ns.imp.system.api.entity.StUser
import com.terra.ns.imp.system.api.enums.UserStatusEnum
import com.terra.ns.imp.system.constant.enums.OrgCheckTypeEnum
import com.terra.ns.imp.system.entity.StDepartment
import com.terra.ns.imp.system.entity.StRole
import com.terra.ns.imp.system.vo.base.UserBaseResponse
import com.terra.ns.imp.system.vo.response.OrgItemResponse
import millsToLocalDateTime
import toLocalDateTime
import toStringFormat10
import toStringFormat19

object UserConvert {
    fun convert(phone: String, loginPassword: String) : StUser {
        val user = StUser()
        user.phone = phone
        user.password = loginPassword
        user.code = IdUtil.getSnowflakeNextIdStr()
        user.status = UserStatusEnum.NORMAL.code
        return user
    }

    fun convert(dingUser: UserV2DTO) : StUser {
        val user = StUser()
        user.code = IdUtil.getSnowflakeNextIdStr()
        user.status = UserStatusEnum.NORMAL.code
        user.phone = dingUser.mobile
        user.name = dingUser.name
        user.avatar = dingUser.avatar
        user.jobNumber = dingUser.avatar
        user.email = dingUser.avatar
        user.orgEmail = dingUser.avatar
        user.hiredDate = dingUser.hiredDate.toLocalDateTime()
        user.position = if (dingUser.title.isNullOrEmpty()) dingUser.position else dingUser.title
        return user
    }

    fun convert(dingUser: UserDetail): StUser {
        val user = StUser()
        user.code = IdUtil.getSnowflakeNextIdStr()
        user.status = UserStatusEnum.NORMAL.code
        user.phone = dingUser.mobile
        user.name = dingUser.name
        user.avatar = dingUser.avatar
        user.jobNumber = dingUser.avatar
        user.email = dingUser.avatar
        user.orgEmail = dingUser.avatar
        dingUser.hiredDate?.let {
            user.hiredDate = dingUser.hiredDate.millsToLocalDateTime()
        }
        user.position = dingUser.title
        return user
    }


    fun convertUserBaseResponse(user: StUser): UserBaseResponse {
        val response = UserBaseResponse()
        response.name = user.name
        response.phone = user.phone
        response.email = user.email
        response.avatar = user.avatar
        response.hiredDate = user.hiredDate?.toStringFormat10()
        response.position = user.position
        response.status = user.status
        response.statusName = UserStatusEnum.getDesc(user.status)
        response.createDate = user.createDate?.toStringFormat19()
        response.modifyDate = user.modifyDate?.toStringFormat19()
        return response
    }


    fun convertOrgItemResponse(user: StUser): OrgItemResponse {
        val response = OrgItemResponse()
        response.title = user.name
        response.key = user.code
        response.job = user.position
        response.type = OrgCheckTypeEnum.USER.type
        return response
    }

    fun convertOrgItemResponse(dept: StDepartment, hasNext: Boolean = false): OrgItemResponse {
        val response = OrgItemResponse()
        response.title = dept.name
        response.key = dept.code
        response.type = OrgCheckTypeEnum.DEPT.type
        response.hasNext = hasNext
        return response
    }

    fun convertOrgItemResponse(role: StRole): OrgItemResponse {
        val response = OrgItemResponse()
        response.title = role.name
        response.key = role.code
        response.type = OrgCheckTypeEnum.ROLE.type
        return response
    }


}