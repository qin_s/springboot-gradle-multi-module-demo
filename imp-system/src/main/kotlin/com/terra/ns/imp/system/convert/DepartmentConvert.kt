package com.terra.ns.imp.system.convert

import com.terra.ns.imp.system.constant.SystemConstants
import com.terra.ns.imp.system.entity.StDepartment
import com.terra.ns.imp.system.vo.base.DeptBaseRequest
import com.terra.ns.imp.system.vo.base.DeptBaseResponse
import com.terra.ns.imp.system.vo.response.DeptTreeResponse

object DepartmentConvert {

    fun convert(request: DeptBaseRequest): StDepartment {
        val dept = StDepartment()
        dept.name = request.name
        dept.parentCode = request.parentCode
        dept.principalCode = request.principalCode
        return dept
    }

    fun convertToBaseResponse(dept: StDepartment): DeptBaseResponse {
        val deptResponse = DeptBaseResponse()
        deptResponse.name = dept.name
        deptResponse.code = dept.code
        deptResponse.parentCode = dept.parentCode
        deptResponse.nodePath = dept.nodePath
        deptResponse.principalCode = dept.principalCode
        deptResponse.topDeptCode = dept.topNodeCode
        return deptResponse
    }

    fun convertDeptTreeResponse(dept: StDepartment) : DeptTreeResponse {
        val deptTree =  DeptTreeResponse()
        deptTree.name = dept.name
        deptTree.code = dept.code
        deptTree.parentCode = dept.parentCode
        deptTree.nodePath = dept.nodePath
        deptTree.principalCode = dept.principalCode
        deptTree.topDeptCode = dept.topNodeCode
        deptTree.children = null
        return deptTree
    }

    fun convertTopStDept(tenantId: Long, companyNameCn: String) : StDepartment {
        val topDept = StDepartment()
        topDept.name = companyNameCn
        topDept.parentCode = SystemConstants.TOP_DEPT_PARENT_CODE
        topDept.topNodeCode = SystemConstants.TOP_DEPT_PARENT_CODE
        return topDept
    }
}