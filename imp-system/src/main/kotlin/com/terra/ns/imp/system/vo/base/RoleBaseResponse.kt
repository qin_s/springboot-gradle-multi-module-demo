package com.terra.ns.imp.system.vo.base

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/6/6
@desc 角色基础信息响应实体
 */
@Schema(description = "角色基础信息响应实体")
open class RoleBaseResponse {

    @Schema(description = "编码")
    var code : String? = null

    @Schema(description = "角色名称")
    var name : String? = null

    @Schema(description = "角色状态")
    var status : Int? = null

    @Schema(description =  "备注")
    var remark: String? = null
}