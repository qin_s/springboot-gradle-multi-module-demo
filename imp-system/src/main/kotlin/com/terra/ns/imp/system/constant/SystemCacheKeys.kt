package com.terra.ns.imp.system.constant

import com.terra.ns.imp.common.core.definition.ConstantDefinition
/**
@author qins
@date 2023/6/16
@desc 系统模块缓存Key
 */
object SystemCacheKeys : ConstantDefinition  {

    const val DEPT_CODES_CACHE_KEY = "dept:codes" // 部门编码缓存key

    const val IMPORT_DEPT_LOCK_KEY = "dept:import:lock" // 导入部门锁key

    const val IMPORT_DEPT_RATE_KEY = "dept:import:rate" // 导入部门进度key

    const val IMPORT_DEPT_TOTAL_KEY = "dept:import:total" // 导入部门总数key

    const val IMPORT_DEPT_SUCCESS_COUNT_KEY = "dept:import:success_count" // 导入部门成功数key

    const val IMPORT_DEPT_NEW_DEPT_COUNT_KEY = "dept:import:new_dept_count" // 导入部门新建部门数key

    const val IMPORT_DEPT_NEW_USER_COUNT_KEY = "dept:import:new_user_count" // 导入部门新建用户数key

    const val IMPORT_DEPT_COVER_USER_COUNT_KEY = "dept:import:cover_user_count" // 导入部门覆盖用户数key

    const val IMPORT_DEPT_SKIP_USER_COUNT_KEY = "dept:import:skip_user_count" // 导入部门忽略用户数key

    const val IMPORT_DEPT_RESULT_DESC_KEY = "dept:import:result_desc" // 导入部门结果描述key

    const val DING_EVENT_HANDLER_LOCK_KEY = "dept:ding:event" // 钉钉事件处理分布式锁key

}