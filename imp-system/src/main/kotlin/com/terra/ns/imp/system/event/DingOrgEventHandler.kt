package com.terra.ns.imp.system.event

import com.terra.ns.imp.common.dingtalk.event.AbstractDingtalkEventListener
import com.terra.ns.imp.common.dingtalk.event.DingEventTypeEnum
import com.terra.ns.imp.common.dingtalk.event.DingtalkEvent
import com.terra.ns.imp.system.service.IDeptSyncService
import jakarta.annotation.Resource
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component

/**
@author qins
@date 2023/11/21
@desc 钉钉用户，部门变更事件处理
 */
@Component
class DingOrgEventHandler : AbstractDingtalkEventListener() {

    private val log = LoggerFactory.getLogger(this::class.java)

    @Resource
    @Qualifier("dingDeptSyncService")
    private lateinit var dingDeptSyncService: IDeptSyncService

    init {
        this.types = setOf(
            DingEventTypeEnum.USER_ADD_ORG,
            DingEventTypeEnum.USER_MODIFY_ORG,
            DingEventTypeEnum.USER_LEAVE_ORG,
            DingEventTypeEnum.ORG_DEPT_CREATE,
            DingEventTypeEnum.ORG_DEPT_MODIFY,
            DingEventTypeEnum.ORG_DEPT_REMOVE
        )
    }

    override fun removeDept(event: DingtalkEvent) {
        dingDeptSyncService.deleteDeptEventHandle(event.data)
    }

    override fun modifyDept(event: DingtalkEvent) {
        dingDeptSyncService.editDeptEventHandle(event.data)
    }

    override fun addDept(event: DingtalkEvent) {
        dingDeptSyncService.addDeptEventHandle(event.data)
    }

    override fun leaveUser(event: DingtalkEvent) {
        dingDeptSyncService.exitUserEventHandle(event.data)
    }

    override fun modifyUser(event: DingtalkEvent) {
        dingDeptSyncService.editUserEventHandle(event.data)
    }

    override fun addUser(event: DingtalkEvent) {
       dingDeptSyncService.addUserEventHandle(event.data)
    }
}