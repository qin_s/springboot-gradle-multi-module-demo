package com.terra.ns.imp.system.vo.base

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/6/14
@desc
 */
@Schema(description = "用户基础信息")
open class UserBaseResponse {

    @Schema(description = "用户名称")
    var name: String? = null

    @Schema(description = "手机号")
    var phone: String? = null

    @Schema(description = "邮箱")
    var email: String? = null

    @Schema(description = "头像地址")
    var avatar: String? = null

    @Schema(description = "入职时间 yyyy-mm-dd")
    var hiredDate: String? = null

    @Schema(description = "职位")
    var position: String? = null

    @Schema(description = "状态")
    var status: Int? = null

    @Schema(description = "状态名称")
    var statusName: String? = null

    @Schema(description = "创建时间")
    var createDate: String? = null

    @Schema(description = "更新时间")
    var modifyDate: String? = null
}