package com.terra.ns.imp.system.vo.request

import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank

/**
@author qins
@date 2023/6/14
@desc 用户分配角色请求
 */
@Schema(description = "关用户分配角色请求体")
data class UserAssignRoleRequest (

    @Schema(description="用户编码", required = true, example = "code")
    @get:NotBlank(message = "用户编码不能为空")
    var code : String = "",

    @Schema(description = "角色编码列表")
    var roleCodeList: List<String> = emptyList()
)