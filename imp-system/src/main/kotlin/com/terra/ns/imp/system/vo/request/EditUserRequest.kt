package com.terra.ns.imp.system.vo.request

import com.terra.ns.imp.system.vo.base.UserBaseRequest
import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank

/**
@author qins
@date 2023/6/14
@desc 编辑用户请求体
 */
@Schema(description = "编辑用户请求体")
data class EditUserRequest(

    @Schema(description = "用户编码", required = true)
    @get:NotBlank(message = "用户编码不能为空")
    var code: String = "",

) : UserBaseRequest()