package com.terra.ns.imp.system.service

import com.terra.ns.imp.common.vo.request.ByCodeRequest
import com.terra.ns.imp.common.vo.request.PageParam
import com.terra.ns.imp.common.vo.request.QueryByKeywordsRequest
import com.terra.ns.imp.common.vo.response.PageResult
import com.terra.ns.imp.system.vo.base.RoleBaseRequest
import com.terra.ns.imp.system.vo.base.RoleBaseResponse
import com.terra.ns.imp.system.vo.request.EditRoleRequest
import com.terra.ns.imp.system.vo.request.RoleAssignPermRequest
import com.terra.ns.imp.system.vo.request.RoleAssignUserRequest
import com.terra.ns.imp.system.vo.response.RolePermResponse

/**
@author qins
@date 2023/6/6
@desc
 */
interface IStRoleService {

    /**
     * 新增角色
     */
    fun addRole(request: RoleBaseRequest)

    /**
     * 更新角色信息
     */
    fun updateRole(request: EditRoleRequest)

    /**
     * 删除角色信息
     */
    fun deleteRole(request: ByCodeRequest)

    /**
     * 给角色关联用户
     */
    fun assignUser(request: RoleAssignUserRequest)

    /**
     * 查询角色列表
     */
    fun selectRoleList(request: QueryByKeywordsRequest): List<RoleBaseResponse>

    /**
     * 分页查询角色
     */
    fun selectRolePage(request: PageParam<QueryByKeywordsRequest>): PageResult<RoleBaseResponse>

    /**
     * 查询角色详细
     */
    fun selectRoleDetail(request: ByCodeRequest): RoleBaseResponse

    /**
     * 给角色分配权限
     */
    fun assignPerm(request: RoleAssignPermRequest)

    /**
     * 查询角色权限
     */
    fun selectRolePerm(request: ByCodeRequest): RolePermResponse
}