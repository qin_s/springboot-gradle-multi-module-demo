package com.terra.ns.imp.system.constant

import com.terra.ns.imp.common.core.definition.AbstractModuleDefinition

class SystemModule private constructor(code: String = SystemConstants.MODULE_CODE,
                                       name: String = "系统用户",
                                       desc: String = "系统用户管理模块"): AbstractModuleDefinition(code, name, desc) {
    companion object {
         val instance = SingleHolder.instance
    }
    private object SingleHolder {
        val instance = SystemModule()
    }
}
