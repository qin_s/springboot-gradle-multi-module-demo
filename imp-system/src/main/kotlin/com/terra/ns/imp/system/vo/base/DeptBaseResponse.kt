package com.terra.ns.imp.system.vo.base

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2022/4/9
@desc 部门基础信息响应实体
 */
@Schema(description = "部门基础信息响应实体")
open class DeptBaseResponse {

    @Schema(description = "编码")
    var code : String? = null

    @Schema(description = "部门名称")
    var name : String? = null

    @Schema(description = "父部门编码")
    var parentCode : String? = null

    @Schema(description = "顶级部门编码")
    var topDeptCode: String? = null

    @Schema(description = "部门层级路径")
    var nodePath: String? = null

    @Schema(description = "负责人编码")
    var principalCode: String? = null

//    @Schema(description = "负责人名称")
//    var principalName: String? = null

//    @Schema(description = "绑定扩展字段类型，0：无绑定 1：钉钉")
//    var extBindType: Int = StructureExtBindTypeEnum.NOT_BIND_EXT.code

}