package com.terra.ns.imp.system.convert

import com.terra.ns.imp.system.constant.enums.UserDepRelMainDeptEnum
import com.terra.ns.imp.system.entity.StUserDeptRel

object UserDeptRelConvert {

    fun convert(userCode: String, deptCode: String, mainDeptCode: String?): StUserDeptRel {
        val rel = StUserDeptRel()
        rel.userCode = userCode
        rel.departmentCode = deptCode
        rel.mainDept = if(deptCode == mainDeptCode) UserDepRelMainDeptEnum.IS_MAIN_DEPT.code else UserDepRelMainDeptEnum.NOT_MAIN_DEPT.code
        return rel
    }
}