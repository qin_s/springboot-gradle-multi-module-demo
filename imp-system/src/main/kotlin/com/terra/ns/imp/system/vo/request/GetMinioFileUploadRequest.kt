package com.terra.ns.imp.system.vo.request

import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank

/**
@author qins
@date 2023/8/10
@desc
 */
@Schema(description = "获取文件上传url请求")
data class GetMinioFileUploadRequest(

    @Schema(description = "文件名全称，包含后缀", required = true)
    @get:NotBlank(message = "文件名称不能为空")
    var fileName: String = ""
)