package com.terra.ns.imp.system.mapper

import com.terra.ns.imp.common.mybatis.mapper.BaseMapperX
import com.terra.ns.imp.common.mybatis.mapper.KtQueryWrapperX
import com.terra.ns.imp.system.entity.StUserDingExt

/**
 * <p>
 * 钉钉用户扩展属性表 Mapper 接口
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
interface StUserDingExtMapper : BaseMapperX<StUserDingExt> {

    fun selectListByRelCodes(userCodes: List<String>): List<StUserDingExt> {
        if(userCodes.isEmpty()) return emptyList()
        return selectList(KtQueryWrapperX(StUserDingExt::class.java).inX(StUserDingExt::userCode, userCodes))
    }

    fun selectByMobileList(phones: List<String>): List<StUserDingExt> {
        if(phones.isEmpty()) return emptyList()
        return selectList(KtQueryWrapperX(StUserDingExt::class.java).inX(StUserDingExt::mobile, phones))
    }

    fun selectOneByUserId(userid: String): StUserDingExt?
        = selectOne(KtQueryWrapperX(StUserDingExt::class.java).eqX(StUserDingExt::userid, userid))

    fun updateAllFieldById(updateDingUserExt: StUserDingExt, dataId: Long)
        = updateFieldIncludeNull(updateDingUserExt, KtQueryWrapperX(StUserDingExt::class.java).eqX(StUserDingExt::id, dataId))

    fun selectListByUserIds(dingUserIds: List<String>): List<StUserDingExt> {
        if(dingUserIds.isEmpty()) return emptyList()
        return selectList(KtQueryWrapperX(StUserDingExt::class.java).inX(StUserDingExt::userid, dingUserIds))
    }
}
