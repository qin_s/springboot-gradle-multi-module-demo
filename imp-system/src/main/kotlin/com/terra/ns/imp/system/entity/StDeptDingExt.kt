package com.terra.ns.imp.system.entity

import com.baomidou.mybatisplus.annotation.TableName
import com.terra.ns.imp.common.vo.entity.BaseTableField

/**
 * <p>
 * 钉钉部门扩展信息表
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
@TableName("st_dept_ding_ext")
class StDeptDingExt : BaseTableField() {


    /**
     * 钉钉部门ID
     */
    var deptId: Long? = null

    /**
     * 部门名称
     */
    var name: String? = null

    /**
     * 父部门ID（为1表示为根部门）
     */
    var parentId: Long? = null

    /**
     * 部门唯一自定义标识（可使用统一编码关联）
     */
    var sourceIdentifier: String? = null

    /**
     * 是否同步创建一个关联此部门的企业群：0：不创建 ; 1：创建
     */
    var createDeptGroup: Int? = null

    /**
     * 当部门群已经创建后，是否有新人加入部门会自动加入该群：0:不加入；1,加入
     */
    var autoAddUser: Int? = null

    /**
     * 是否默认同意加入该部门的申请：0，需审批；1，默认同意
     */
    var autoApproveApply: Int? = null

    /**
     * 部门是否来自关联组织:0：否；1，是
     */
    var fromUnionOrg: Int? = null

    /**
     * 教育部门标签：campus：校区;period：学段;grade：年级;class：班级
     */
    var tags: String? = null

    /**
     * 在父部门中的次序值(小者靠前)
     */
    var orderNum: Long? = null

    /**
     * 部门群ID。
     */
    var deptGroupChatId: String? = null

    /**
     * 部门群是否包含子部门：0：否；1：是
     */
    var groupContainSubDept: Int? = null

    /**
     * 企业群群主userId。
     */
    var orgDeptOwner: String? = null

    /**
     * 部门主管userId列表（以"，"分割）
     */
    var deptManagerUseridList: String? = null

    /**
     * 是否限制本部门成员查看通讯录：0:不限制；1：限制只能看限定范围内的通讯录
     */
    var outerDept: Int? = null

    /**
     * OUTER_DEPT = 1时，部门员工可见部门ID列表，（以"，"分割）
     */
    var outerPermitDepts: String? = null

    /**
     * OUTER_DEPT = 1时，部门员工可见员工ID列表，（以"，"分割）
     */
    var outerPermitUsers: String? = null

    /**
     * 是否隐藏本部门：0:，不隐藏；1：隐藏（不显示在公司通讯录）
     */
    var hideDept: Int? = null

    /**
     * HIDE_DEPT = 1时，允许查看本部门的用户ID列表
     */
    var userPermits: String? = null

    /**
     * HIDE_DEPT = 1时，允许查看本部门的部门ID列表
     */
    var deptPermits: String? = null
}
