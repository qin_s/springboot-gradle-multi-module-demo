package com.terra.ns.imp.system.service

import com.terra.micro.portal.common.service.vo.organization.user.request.EditPasswordRequest
import com.terra.ns.imp.common.vo.request.ByCodeRequest
import com.terra.ns.imp.common.vo.request.PageParam
import com.terra.ns.imp.common.vo.request.QueryByKeywordsRequest
import com.terra.ns.imp.common.vo.response.PageResult
import com.terra.ns.imp.system.api.entity.StUser
import com.terra.ns.imp.system.vo.base.UserBaseResponse
import com.terra.ns.imp.system.vo.request.*
import com.terra.ns.imp.system.vo.response.OrgItemResponse
import com.terra.ns.imp.system.vo.response.UserDetailResponse

/**
@author qins
@date 2023/6/14
@desc
 */
interface IStUserService {

    /**
     * 分页查询用户列表
     */
    fun selectUserPage(request: PageParam<QueryByKeywordsRequest>): PageResult<UserDetailResponse>

    /**
     * 新增用户
     */
    fun addUser(request: AddUserRequest)

    /**
     * 编辑用户
     */
    fun updateUser(request: EditUserRequest)

    /**
     * 删除租户用户
     */
    fun deleteUser(request: ByCodeRequest)

    /**
     * 为用户分配角色
     */
    fun assignRole(request: UserAssignRoleRequest)

    /**
     * 修改当前登录人密码
     */
    fun modifyPwd(request: EditPasswordRequest)

    /**
     * 重置用户密码
     */
    fun resetPassword(request: ByCodeRequest)

    /**
     * 忘记密码
     */
    fun forgetLoginPassword(request: ForgetPasswordRequest)

    /**
     * 获取用户详细信息，包含部门，角色数据
     */
    fun getDetailUser(request: ByCodeRequest): UserDetailResponse

    /**
     * 获取用户列表，不含关联数据
     */
    fun getUserList(request: QueryByKeywordsRequest): List<UserBaseResponse>

    /**
     * 获取部门下所有用户
     */
    fun getUserByDept(request: ByCodeRequest): List<UserBaseResponse>

    /**
     * 获取角色下所有用户
     */
    fun getUserByRole(request: ByCodeRequest): List<UserBaseResponse>

    /**
     * 生成用户密码
     */
    fun encodePassword(password: String?): String

    /**
     * 检索部门/用户/角色
     */
    fun queryOrgList(request: QueryOrgRequest): List<OrgItemResponse>


    /**
     * 断言：用户状态正常
     */
    fun requireStatusNormal(user: StUser)

}