package com.terra.ns.imp.system.service

import com.alibaba.fastjson2.JSONObject
import com.terra.ns.imp.common.redis.utils.RedisUtils
import com.terra.ns.imp.system.constant.SystemCacheKeys
import com.terra.ns.imp.system.constant.enums.DeptSyncTypeEnum

/**
@author qins
@date 2023/6/16
@desc 定义部门同步方法（钉钉/飞书/..)
 */
interface IDeptSyncService {

    /**
     * 同步部门动作。由调用者决定同步还是异步
     * 和调用者独立service的方法调用，保证事务生效
     * @param syncType 同步类型
     */
    fun doSyncDept(syncType: DeptSyncTypeEnum)

    /**
     * 判断是否能够同步部门, 提供默认实现
     */
    fun enableSyncDept(): Boolean {
        if(RedisUtils.hasKey(SystemCacheKeys.IMPORT_DEPT_RATE_KEY)) {
            val rate = RedisUtils.get<Int>(SystemCacheKeys.IMPORT_DEPT_RATE_KEY) ?: -1
            if(rate in 0..99) {
                return false
            }
        }
        return true
    }

    /**
     * 初始化缓存的同步信息, 提供默认实现
     */
    fun initSyncDeptCache() {
        updateSyncDeptCacheItems(mapOf(
            Pair(SystemCacheKeys.IMPORT_DEPT_RATE_KEY, 0),
            Pair(SystemCacheKeys.IMPORT_DEPT_TOTAL_KEY, 0),
            Pair(SystemCacheKeys.IMPORT_DEPT_SUCCESS_COUNT_KEY, 0),
            Pair(SystemCacheKeys.IMPORT_DEPT_NEW_DEPT_COUNT_KEY, 0),
            Pair(SystemCacheKeys.IMPORT_DEPT_NEW_USER_COUNT_KEY, 0),
            Pair(SystemCacheKeys.IMPORT_DEPT_COVER_USER_COUNT_KEY, 0),
            Pair(SystemCacheKeys.IMPORT_DEPT_SKIP_USER_COUNT_KEY, 0),
            Pair(SystemCacheKeys.IMPORT_DEPT_RESULT_DESC_KEY, ""),
        ))
    }

    /**
     * 更新多项同步缓存的数据, 提供默认实现
     * @param updateItems 更新项及数据，
     * example:
     *      <SystemCacheKeys.IMPORT_DEPT_RATE_KEY, 100>
     *      <SystemCacheKeys.IMPORT_DEPT_RESULT_DESC_KEY, "成功">
     */
    fun updateSyncDeptCacheItems(updateItems: Map<String, Any>) {
        updateItems.entries.forEach {
            RedisUtils.set(it.key, it.value)
        }
    }

    /**
     * 更新单项同步缓存的数据, 提供默认实现
     * @param fullKey 完整key
     * @param value 缓存值
     */
    fun updateSyncDeptCacheItem(fullKey: String, value: Any) {
        RedisUtils.set(fullKey, value)
    }

    /**
     * 自增单项同步缓存的数据, 提供默认实现
     * @param fullKey 完整key
     */
    fun incrementSyncDeptCacheItem(fullKey: String) {
        if(RedisUtils.hasKey(fullKey)) {
            RedisUtils.set(fullKey, (RedisUtils.get<Int>(fullKey) ?: 0) + 1)
        }
    }

    /**
     * 新增用户事件处理逻辑
     * @param eventJson 事件内容
     */
    fun addUserEventHandle(eventJson: JSONObject)

    /**
     * 修改用户事件处理逻辑
     * @param eventJson 事件内容
     */
    fun editUserEventHandle(eventJson: JSONObject)

    /**
     * 离职用户事件处理逻辑
     * @param eventJson 事件内容
     */
    fun exitUserEventHandle(eventJson: JSONObject)

    /**
     * 新增部门事件处理逻辑
     * @param eventJson 事件内容
     */
    fun addDeptEventHandle(eventJson: JSONObject)

    /**
     * 修改部门事件处理逻辑
     * @param eventJson 事件内容
     */
    fun editDeptEventHandle(eventJson: JSONObject)

    /**
     * 删除部门事件处理逻辑
     * @param eventJson 事件内容
     */
    fun deleteDeptEventHandle(eventJson: JSONObject)

}