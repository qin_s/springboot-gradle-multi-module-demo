package com.terra.ns.imp.system.entity

import com.baomidou.mybatisplus.annotation.TableName
import com.terra.ns.imp.common.vo.entity.BaseTableField

/**
 * <p>
 * 租户用户角色关系表
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
@TableName("st_user_role_rel")
class StUserRoleRel : BaseTableField() {

    /**
     * 用户编码
     */
    var userCode: String? = null

    /**
     * 角色编码
     */
    var roleCode: String? = null
}
