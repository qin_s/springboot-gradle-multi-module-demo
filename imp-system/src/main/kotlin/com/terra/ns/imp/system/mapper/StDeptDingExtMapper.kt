package com.terra.ns.imp.system.mapper

import com.terra.ns.imp.common.mybatis.mapper.BaseMapperX
import com.terra.ns.imp.common.mybatis.mapper.KtQueryWrapperX
import com.terra.ns.imp.system.entity.StDeptDingExt

/**
 * <p>
 * 钉钉部门扩展信息表 Mapper 接口
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
interface StDeptDingExtMapper : BaseMapperX<StDeptDingExt> {

    fun deleteByDeptCode(deptCode: String)
        = delete(KtQueryWrapperX(StDeptDingExt::class.java).eqX(StDeptDingExt::sourceIdentifier, deptCode))

    fun selectOneByDingDeptId(deptId: Long): StDeptDingExt?
        = selectOne(KtQueryWrapperX(StDeptDingExt::class.java).eqX(StDeptDingExt::deptId, deptId).lastLimit1())

    fun selectOneByDingDeptIds(deptIds: List<Long>): List<StDeptDingExt> {
        if(deptIds.isEmpty()) return emptyList()
        return selectList(KtQueryWrapperX(StDeptDingExt::class.java).inX(StDeptDingExt::deptId, deptIds))
    }

    fun selectListBySourceIdentifierList(stDeptCodes: List<String>) : List<StDeptDingExt>
        = selectList(KtQueryWrapperX(StDeptDingExt::class.java).inX(StDeptDingExt::sourceIdentifier, stDeptCodes))


    fun updateAllFieldByDingDeptId(newDingExt: StDeptDingExt, dingDeptId: Long, ignoreColumn: List<String>)
        = updateFieldIncludeNull(newDingExt, KtQueryWrapperX(StDeptDingExt::class.java).eqX(StDeptDingExt::deptId, dingDeptId), ignoreColumn)
}
