package com.terra.ns.imp.system.vo.response

import com.terra.ns.imp.system.vo.base.DeptBaseResponse
import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/6/6
@desc 部门树实体
 */
@Schema(description = "部门树实体")
data class DeptTreeResponse (

    @Schema(description = "子部门列表")
    var children: List<DeptTreeResponse>? = null

) : DeptBaseResponse()