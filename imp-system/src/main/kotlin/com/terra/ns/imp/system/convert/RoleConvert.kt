package com.terra.ns.imp.system.convert

import cn.hutool.core.util.IdUtil
import com.terra.ns.imp.system.entity.StRole
import com.terra.ns.imp.system.vo.base.RoleBaseRequest
import com.terra.ns.imp.system.vo.base.RoleBaseResponse

/**
@author qins
@date 2023/6/8
@desc
 */
object RoleConvert {

    fun convert(request: RoleBaseRequest): StRole {
        val stRole = StRole()
        stRole.code = IdUtil.getSnowflakeNextIdStr()
        stRole.name = request.name
        stRole.remark = request .remark
        return stRole
    }

    fun convertToBaseResponse(role: StRole) : RoleBaseResponse {
        val roleBaseResponse = RoleBaseResponse()
        roleBaseResponse.code = role.code
        roleBaseResponse.name = role.name
        roleBaseResponse.status = role.status
        roleBaseResponse.remark = role.remark
        return roleBaseResponse
    }

}