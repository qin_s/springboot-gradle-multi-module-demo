package com.terra.ns.imp.system.convert

import cn.hutool.core.bean.BeanUtil
import com.terra.ns.imp.common.dingtalk.contact.domain.DepartmentDetailDTO
import com.terra.ns.imp.system.constant.SystemConstants
import com.terra.ns.imp.system.entity.StDeptDingExt

object DeptDingExtConvert {
    fun convert(dingDept: DepartmentDetailDTO): StDeptDingExt {
        val stDeptDingExt = StDeptDingExt()
        BeanUtil.copyProperties(dingDept, stDeptDingExt)
        stDeptDingExt.id = null
        stDeptDingExt.deptId = dingDept.id
        stDeptDingExt.orderNum = dingDept.order
        stDeptDingExt.parentId = dingDept.parentid ?: SystemConstants.DING_TOP_DEPT_ID
        return stDeptDingExt
    }
}