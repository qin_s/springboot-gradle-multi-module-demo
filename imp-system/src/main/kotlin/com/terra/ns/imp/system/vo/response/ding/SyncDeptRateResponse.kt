package com.terra.ns.imp.system.vo.response.ding

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/6/16
@desc 钉钉同步进度响应
 */
@Schema(description = "获取钉钉同步进度响应实体")
data class SyncDeptRateResponse(

    @Schema(description = "同步比例整数，单位：%")
    var syncDepRate: Int = 0,

    @Schema(description = "数据总量")
    var dataTotalCount: Int = 0,

    @Schema(description = "当前已成功数据量")
    var dataSuccessCount: Int = 0,

    @Schema(description = "当前新创建部门量")
    var newDeptCount: Int = 0,

    @Schema(description = "当前新创建成员量")
    var newUserCount: Int = 0,

    @Schema(description = "当前覆盖成员量")
    var coverUserCount: Int = 0,

    @Schema(description = "当前忽略成员量")
    var skipUserCount: Int = 0,

    @Schema(description = "操作结果详情描述")
    var resultDetail: String = ""
)