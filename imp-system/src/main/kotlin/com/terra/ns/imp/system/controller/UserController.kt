package com.terra.ns.imp.system.controller

import com.terra.micro.portal.common.service.vo.organization.user.request.EditPasswordRequest
import com.terra.ns.imp.common.log.OperateLog
import com.terra.ns.imp.common.log.OperateTypeEnum
import com.terra.ns.imp.common.vo.request.ByCodeRequest
import com.terra.ns.imp.common.vo.request.PageParam
import com.terra.ns.imp.common.vo.request.QueryByKeywordsRequest
import com.terra.ns.imp.common.vo.response.PageResult
import com.terra.ns.imp.common.vo.response.ResultBean
import com.terra.ns.imp.common.web.idempotent.Idempotent
import com.terra.ns.imp.system.constant.SystemConstants
import com.terra.ns.imp.system.service.IStUserService
import com.terra.ns.imp.system.vo.base.UserBaseResponse
import com.terra.ns.imp.system.vo.request.*
import com.terra.ns.imp.system.vo.response.OrgItemResponse
import com.terra.ns.imp.system.vo.response.UserDetailResponse
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.validation.Valid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
@author qins
@date 2023/6/6
@desc 用户管理接口
 */
@RequestMapping("/user")
@RestController
@Tag(name = "user")
class UserController {

    @Autowired
    private lateinit var userService: IStUserService

    @PostMapping("/getUserPage")
    @Operation(summary = "分页查询用户列表")
    fun getUserPage(@RequestBody request: PageParam<QueryByKeywordsRequest>): ResultBean<PageResult<UserDetailResponse>> {
        return ResultBean.success(userService.selectUserPage(request))
    }

    @PostMapping("/addUser")
    @Operation(summary = "新增用户")
    @OperateLog(SystemConstants.MODULE_CODE, OperateTypeEnum.ADD)
    @Idempotent
    fun addUser(@RequestBody @Valid request: AddUserRequest): ResultBean<Any> {
        userService.addUser(request)
        return ResultBean.success()
    }

    @PostMapping("/editUser")
    @Operation(summary = "编辑用户")
    @OperateLog(SystemConstants.MODULE_CODE, OperateTypeEnum.MODIFY)
    @Idempotent
    fun editUser(@RequestBody @Valid request: EditUserRequest): ResultBean<Any> {
        userService.updateUser(request)
        return ResultBean.success()
    }

    @PostMapping("/deleteUser")
    @Operation(summary = "删除租户用户", description = "根据用户编码")
    @OperateLog(SystemConstants.MODULE_CODE, OperateTypeEnum.DELETE)
    @Idempotent
    fun deleteUser(@RequestBody @Valid request: ByCodeRequest): ResultBean<Any> {
        userService.deleteUser(request)
        return ResultBean.success()
    }

    @PostMapping("/getUserDetail")
    @Operation(summary = "获取用户详细信息", description = "根据用户编码")
    fun getUserDetail(@RequestBody @Valid request: ByCodeRequest): ResultBean<UserDetailResponse> {
        return ResultBean.success(userService.getDetailUser(request))
    }

    @PostMapping("/getUserList")
    @Operation(summary = "获取用户列表")
    fun getUserList(@RequestBody @Valid request: QueryByKeywordsRequest): ResultBean<List<UserBaseResponse>> {
        return ResultBean.success(userService.getUserList(request))
    }

    @PostMapping("/getUserByDept")
    @Operation(summary = "获取部门下所有用户", description = "根据用户编码")
    fun getUserByDept(@RequestBody @Valid request: ByCodeRequest): ResultBean<List<UserBaseResponse>> {
        return ResultBean.success(userService.getUserByDept(request))
    }

    @PostMapping("/getUserByRole")
    @Operation(summary = "获取角色下所有用户", description = "根据角色编码")
    fun getUserByRole(@RequestBody @Valid request: ByCodeRequest): ResultBean<List<UserBaseResponse>> {
        return ResultBean.success(userService.getUserByRole(request))
    }

    @PostMapping("/assignRole")
    @Operation(summary = "给用分配角色")
    @OperateLog(SystemConstants.MODULE_CODE, OperateTypeEnum.OTHER, "给用分配角色")
    @Idempotent
    fun assignRole(@RequestBody @Valid request: UserAssignRoleRequest): ResultBean<Any> {
        userService.assignRole(request)
        return ResultBean.success()
    }

    @PostMapping("/modifyPwd")
    @Operation(summary = "修改自己密码")
    @OperateLog(SystemConstants.MODULE_CODE, OperateTypeEnum.OTHER, "修改自己密码")
    @Idempotent
    fun modifyPwd(@RequestBody @Valid request: EditPasswordRequest): ResultBean<Any> {
        userService.modifyPwd(request)
        return ResultBean.success()
    }

    @PostMapping("/resetPassword")
    @Operation(summary = "重置密码", description = "根据用户编码")
    @OperateLog(SystemConstants.MODULE_CODE, OperateTypeEnum.OTHER, "重置用户密码")
    @Idempotent
    fun resetPassword(@RequestBody @Valid request: ByCodeRequest): ResultBean<Any> {
        userService.resetPassword(request)
        return ResultBean.success()
    }

    @PostMapping("forgetLoginPassword")
    @Operation(summary = "修改密码(忘记密码)")
    @Idempotent
    @OperateLog(SystemConstants.MODULE_CODE, OperateTypeEnum.OTHER, "修改密码(忘记密码)")
    fun forgetLoginPassword(@RequestBody @Valid request: ForgetPasswordRequest): ResultBean<Any> {
        userService.forgetLoginPassword(request)
        return ResultBean.success()
    }

    @PostMapping("/queryOrgList")
    @Operation(summary = "检索企业部门-人员-角色")
    fun queryOrgList(@RequestBody @Valid request: QueryOrgRequest): ResultBean<List<OrgItemResponse>> {
        return ResultBean.success(userService.queryOrgList(request))
    }

    
}