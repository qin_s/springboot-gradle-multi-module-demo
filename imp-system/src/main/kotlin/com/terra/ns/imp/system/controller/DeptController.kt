package com.terra.ns.imp.system.controller

import com.terra.ns.imp.common.log.OperateLog
import com.terra.ns.imp.common.log.OperateTypeEnum
import com.terra.ns.imp.common.vo.request.ByCodeRequest
import com.terra.ns.imp.common.vo.response.ResultBean
import com.terra.ns.imp.common.web.idempotent.Idempotent
import com.terra.ns.imp.system.constant.SystemConstants.MODULE_CODE
import com.terra.ns.imp.system.service.IStDepartmentService
import com.terra.ns.imp.system.vo.base.DeptBaseRequest
import com.terra.ns.imp.system.vo.base.DeptBaseResponse
import com.terra.ns.imp.system.vo.request.EditDeptRequest
import com.terra.ns.imp.system.vo.request.QueryDeptTreeRequest
import com.terra.ns.imp.system.vo.response.DeptTreeResponse
import com.terra.ns.imp.system.vo.response.ding.SyncDeptRateResponse
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.validation.Valid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
@author qins
@date 2023/6/6
@desc 部门管理接口
 */
@RequestMapping("/dept")
@RestController
@Tag(name = "dept")
class DeptController {

    @Autowired
    private lateinit var departmentService: IStDepartmentService

    @PostMapping("/addDept")
    @Operation(summary = "新增部门")
    @OperateLog(MODULE_CODE, OperateTypeEnum.ADD)
    @Idempotent
    fun addDept(@RequestBody @Valid request: DeptBaseRequest): ResultBean<String> {
        val deptCode = departmentService.addDept(request)
        return ResultBean.success(deptCode)
    }

    @PostMapping("/editDept")
    @Operation(summary = "编辑部门")
    @OperateLog(MODULE_CODE, OperateTypeEnum.MODIFY)
    @Idempotent
    fun editDept(@RequestBody @Valid request: EditDeptRequest): ResultBean<Any> {
        departmentService.updateDept(request)
        return ResultBean.success()
    }

    @PostMapping("/deleteDept")
    @Operation(summary = "删除部门", description = "根据部门编码")
    @OperateLog(MODULE_CODE, OperateTypeEnum.DELETE)
    @Idempotent
    fun deleteDept(@RequestBody @Valid request: ByCodeRequest): ResultBean<Any> {
        departmentService.deleteDept(request)
        return ResultBean.success()
    }

    @PostMapping("/getDeptDetail")
    @Operation(summary = "获取部门详情", description = "根据部门编码")
    fun getDeptDetail(@RequestBody @Valid request: ByCodeRequest): ResultBean<DeptBaseResponse> {
        return ResultBean.success(departmentService.selectDetail(request))
    }

    @PostMapping("/getDeptTree")
    @Operation(summary = "获取部门树")
    fun getDeptTree(@RequestBody @Valid request: QueryDeptTreeRequest): ResultBean<List<DeptTreeResponse>> {
        return ResultBean.success(departmentService.selectDeptTree(request))
    }

    @PostMapping("/syncDingDept")
    @Operation(summary = "主动同步钉钉部门信息")
    @OperateLog(MODULE_CODE, OperateTypeEnum.OTHER, "手动同步钉钉部门")
    @Idempotent
    fun syncDingDept(): ResultBean<Any> {
        departmentService.syncDingDept()
        return ResultBean.success()
    }

    @PostMapping("/getSyncDepRate")
    @Operation(summary = "获取组织架构同步进度")
    fun getSyncDepRate(): ResultBean<SyncDeptRateResponse> {
        return ResultBean.success(departmentService.getSyncDeptRate())
    }

}