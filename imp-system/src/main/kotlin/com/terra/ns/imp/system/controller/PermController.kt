package com.terra.ns.imp.system.controller

import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
@author qins
@date 2023/6/6
@desc 权限管理接口
 */
@RequestMapping("/perm")
@RestController
@Tag(name = "perm")
class PermController {
}