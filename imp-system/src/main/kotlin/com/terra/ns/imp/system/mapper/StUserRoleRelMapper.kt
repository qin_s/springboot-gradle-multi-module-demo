package com.terra.ns.imp.system.mapper

import com.baomidou.mybatisplus.extension.kotlin.KtQueryWrapper
import com.terra.ns.imp.common.mybatis.mapper.BaseMapperX
import com.terra.ns.imp.common.mybatis.mapper.KtQueryWrapperX
import com.terra.ns.imp.system.entity.StUserRoleRel

/**
 * <p>
 * 租户用户角色关系表 Mapper 接口
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
interface StUserRoleRelMapper : BaseMapperX<StUserRoleRel> {

    fun selectListByUserCode(userCode: String) : List<StUserRoleRel>
        = selectList(KtQueryWrapperX(StUserRoleRel::class.java).eqX(StUserRoleRel::userCode, userCode))

    fun deleteByRoleCode(roleCode: String)
        = delete(KtQueryWrapperX(StUserRoleRel::class.java).eqX(StUserRoleRel::roleCode, roleCode))

    fun selectListByRoleCodeAndTenantId(roleCode: String): List<StUserRoleRel>
        = selectList(KtQueryWrapperX(StUserRoleRel::class.java).eqX(StUserRoleRel::roleCode, roleCode))

    fun selectListByRoleCodes(roleCodes: List<String>): List<StUserRoleRel>{
        if(roleCodes.isEmpty()) return emptyList()
        return selectList(KtQueryWrapperX(StUserRoleRel::class.java).inX(StUserRoleRel::roleCode, roleCodes))
    }


    fun deleteByUserCodesAndRoleCodes(userCodes: List<String>, roleCodes: List<String>) {
        if(userCodes.isEmpty() || roleCodes.isEmpty()) return
        delete(KtQueryWrapper(StUserRoleRel::class.java).`in`(StUserRoleRel::userCode, userCodes).`in`(StUserRoleRel::roleCode, roleCodes))
    }

    fun deleteByUserCode(userCode: String)
        = delete(KtQueryWrapperX(StUserRoleRel::class.java).eqX(StUserRoleRel::userCode, userCode))

    fun deleteByUserCodes(delUserCodeList: List<String>) {
        if(delUserCodeList.isEmpty()) return
        delete(KtQueryWrapperX(StUserRoleRel::class.java).inX(StUserRoleRel::userCode, delUserCodeList))
    }

    fun selectListByUserCodes(userCodes: List<String>): List<StUserRoleRel>{
        if(userCodes.isEmpty()) return emptyList()
        return selectList(KtQueryWrapperX(StUserRoleRel::class.java).inX(StUserRoleRel::userCode, userCodes))
    }

}
