package com.terra.ns.imp.system.vo.request

import com.terra.ns.imp.system.vo.base.UserBaseRequest
import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank

/**
@author qins
@date 2023/6/14
@desc 新增用户请求体
 */
@Schema(description = "新增用户请求")
data class AddUserRequest(

    @Schema(description = "手机号", required = true)
    @get:NotBlank(message = "手机号不能为空")
    var phone: String = "",

) : UserBaseRequest()