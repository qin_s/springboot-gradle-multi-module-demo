package com.terra.ns.imp.system.vo.response

import com.terra.ns.imp.system.vo.base.PermBaseResponse
import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 2023/6/14
@desc 权限树响应体
 */
@Schema(description = "权限树实体")
data class PermTreeResponse (

    @Schema(description = "子权限列表")
    var children: List<PermTreeResponse>? = null

) : PermBaseResponse()