package com.terra.ns.imp.system.vo.base

import io.swagger.v3.oas.annotations.media.Schema

/**
@author qins
@date 20223/6/14
@desc 权限基础信息响应实体
 */
@Schema(description = "权限基础信息响应体")
open class PermBaseResponse {

    @Schema(description ="权限名称")
    var name : String? = null

    @Schema(description = "权限别名")
    var alias: String? = null

    @Schema(description = "图标ICON")
    var iconName: String? = null

    @Schema(description = "类型")
    var type: Int? = null

    @Schema(description = "类型名称")
    var typeName: String? = null

    @Schema(description = "排序")
    var sortNum: Int? = null

    @Schema(description = "跳转路由")
    var url: String? = null

    @Schema(description = "组件路径")
    var componentPath: String? = null

    @Schema(description="编码")
    var code : String? = null

    @Schema(description = "父权限编码")
    var parentCode : String? = null

    @Schema(description = "顶级权限编码")
    var topNodeCode: String? = null

    @Schema(description = "权限层级路径")
    var nodePath: String? = null

    @Schema(description = "备注")
    var remark: String? = null
}