package com.terra.ns.imp.system.entity

import com.baomidou.mybatisplus.annotation.TableName
import com.terra.ns.imp.common.vo.entity.BaseTableOperatorField

/**
 * <p>
 * 角色信息表
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
@TableName("st_role")
class StRole : BaseTableOperatorField() {


    /**
     * 角色唯一编码
     */
    var code: String? = null

    /**
     * 角色名称
     */
    var name: String? = null

    /**
     * 备注
     */
    var remark: String? = null

    /**
     * 状态信息（0：正常）
     */
    var status: Int? = null
}
