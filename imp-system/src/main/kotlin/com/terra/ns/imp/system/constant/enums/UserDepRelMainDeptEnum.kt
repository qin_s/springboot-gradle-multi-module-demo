package com.terra.ns.imp.system.constant.enums

/**
@author qins
@date 2023/6/14
@desc 是否主部门
 */
enum class UserDepRelMainDeptEnum(val code: Int, val desc: String) {
    NOT_MAIN_DEPT(0,"非主部门"),
    IS_MAIN_DEPT(1,"主部门"),
    ;
}