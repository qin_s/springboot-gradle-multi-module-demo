package com.terra.ns.imp.system.service;

import com.terra.ns.imp.common.vo.request.ByCodeRequest
import com.terra.ns.imp.system.vo.base.DeptBaseRequest
import com.terra.ns.imp.system.vo.base.DeptBaseResponse
import com.terra.ns.imp.system.vo.request.EditDeptRequest
import com.terra.ns.imp.system.vo.request.QueryDeptTreeRequest
import com.terra.ns.imp.system.vo.response.DeptTreeResponse
import com.terra.ns.imp.system.vo.response.ding.SyncDeptRateResponse

/**
 * @author qins
 * @date 2023/06/05
 */
interface IStDepartmentService {

    /**
     * 新增部门
     */
    fun addDept(request: DeptBaseRequest): String

    /**
     * 编辑部门
     */
    fun updateDept(request: EditDeptRequest)

    /**
     * 删除部门，不能删除有子部门的部门
     */
    fun deleteDept(request: ByCodeRequest)

    /**
     * 查询部门详情
     */
    fun selectDetail(request: ByCodeRequest): DeptBaseResponse

    /**
     * 查询部门树
     */
    fun selectDeptTree(request: QueryDeptTreeRequest): List<DeptTreeResponse>

    /**
     * 根据部门名称搜索只返回匹配的部门及其所有上级的部门
     */
    fun selectDeptTreeByName(name: String): List<DeptTreeResponse>

    /**
     * 手动同步钉钉部门
     */
    fun syncDingDept()

    /**
     * 获取组织架构同步进度
     */
    fun getSyncDeptRate(): SyncDeptRateResponse
}
