package com.terra.ns.imp.system.mapper

import com.terra.ns.imp.common.mybatis.mapper.BaseMapperX
import com.terra.ns.imp.common.mybatis.mapper.KtQueryWrapperX
import com.terra.ns.imp.system.api.entity.StUser
import com.terra.ns.imp.system.api.enums.UserStatusEnum

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
interface StUserMapper : BaseMapperX<StUser> {

    fun selectOneByPhone(phone: String): StUser?
        = selectOne(KtQueryWrapperX(StUser::class.java).eqX(StUser::phone, phone).inX(StUser::status, UserStatusEnum.getNotLogoutStatus()))

    fun selectOneByCode(code: String): StUser?
            = selectOne(KtQueryWrapperX(StUser::class.java).eqX(StUser::code, code))

    fun selectListByPhones(phones: List<String>): List<StUser> {
        if(phones.isEmpty()) return emptyList()
        return selectList(KtQueryWrapperX(StUser::class.java).inX(StUser::phone, phones).inX(StUser::status, UserStatusEnum.getNotLogoutStatus()))
    }

    fun selectListByCodes(userCodes: List<String>): List<StUser> {
        if(userCodes.isEmpty()) return emptyList()
        return selectList(KtQueryWrapperX(StUser::class.java).inX(StUser::code, userCodes).inX(StUser::status, UserStatusEnum.getNotLogoutStatus()))
    }

    fun updateAllFieldByUserCode(updateUser: StUser, code: String, ignoreColumn: List<String>) {
        updateFieldIncludeNull(updateUser, KtQueryWrapperX(StUser::class.java).eq(StUser::code, code), ignoreColumn)
    }

    fun selectAllUser(): List<StUser> {
        return selectList(KtQueryWrapperX(StUser::class.java))
    }

    fun selectListByKeywords(keywords: String?): List<StUser> {
        return selectList(KtQueryWrapperX(StUser::class.java).likeIfPresent(StUser::name, keywords))
    }


}
