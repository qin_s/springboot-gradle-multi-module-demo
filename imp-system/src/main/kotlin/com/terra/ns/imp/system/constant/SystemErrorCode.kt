package com.terra.ns.imp.system.constant

import com.terra.ns.imp.common.core.definition.ErrorCodeDefinition
import com.terra.ns.imp.common.core.exception.ErrorCode

/**
@author qins
@date 2023/6/1
@desc 系统模块错误码 1-20-xxxxx
 */
object SystemErrorCode : ErrorCodeDefinition {

    val PHONE_FORMAT_ERROR = ErrorCode(12000002, "手机号格式错误")
    val CAPTCHA_CREATE_ERROR = ErrorCode(12000003, "获取验证码错误: {}")
    val SMS_TEMPLATE_NOT_EXIST = ErrorCode(12000004, "短信模板不存在")

    val COMPANY_NOT_EXIST = ErrorCode(12010000, "企业不存在")
    val PARENT_DEPT_NOT_EXIST = ErrorCode(12010001, "父级部门不存在")
    val DEPT_NOT_EXIST = ErrorCode(12010002, "部门不存在")
    val TENANT_ID_NOT_MATCH = ErrorCode(12010003, "操作对象不属于当前企业")
    val DEPT_NAME_ERROR = ErrorCode(12010004, "部门名称错误")
    val LEVEL_DEPT_NAME_EXIST = ErrorCode(12010005, "同级部门名称已存在")
    val TOP_DEPT_NAME_CAN_NOT_MODIFY = ErrorCode(12010006, "顶层部门名称必须与企业名称相同")
    val COMPANY_STRUCTURE_CAN_NOT_OPERATE = ErrorCode(12010007, "企业组织架构不允许手动更改")
    val DEL_HAS_CHILDREN_DEPT = ErrorCode(12010008, "存在子部门，不能删除")
    val DEL_HAS_USER_DEPT = ErrorCode(12010009, "部门有成员，不能删除")
    val DING_STRUCTURE_PARAM_ERROR = ErrorCode(12010010, "绑定钉钉参数错误")
    val COMPANY_STRUCTURE_NOT_BIND_DING = ErrorCode(12010011, "组织架构未绑定钉钉")
    val SYNC_DING_DEPT_ERROR = ErrorCode(12010012, "钉钉组织架构同步异常")
    val DING_DEPT_SYNC_ING = ErrorCode(12010013, "正在同步中，请稍后")
    val DING_EVENT_TYPE_INVALID = ErrorCode(12010014, "不处理的钉钉事件类型")
    val DING_CONFIG_CORP_ID_DIFF = ErrorCode(12010015, "钉钉企业ID与绑定的钉钉参数不一致")
    val DING_PARENT_DEPT_NOT_EXIST = ErrorCode(12010016, "未找到钉钉父级部门")

    val ROLE_NOT_EXIST = ErrorCode(12020001, "角色不存在")
    val ROLE_NAME_ERROR = ErrorCode(12020002, "角色名称错误")
    val ROLE_NAME_EXIST = ErrorCode(12020003, "角色名称已存在")

    val USER_STATUS_EXCEPTION_AT_PLATFORM = ErrorCode(12030001, "用户在平台的状态异常")
    val USER_EXIST_IN_COMPANY = ErrorCode(12030002, "该用户已存在于企业中")
    val USER_NOT_EXIST_AT_PLATFORM = ErrorCode(12030003, "用户不存在")
    val USER_OLD_PASSWORD_ERROR = ErrorCode(12030004, "原密码不正确")
}