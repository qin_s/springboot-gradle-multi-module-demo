package com.terra.ns.imp.system.event

import com.terra.ns.imp.system.constant.enums.UserEventEnum

/**
@author qins
@date 2023/6/16
@desc 用户相关事件，可以不再继承ApplicationEvent
 */
class UserEvent(userCodes: List<String>, event: UserEventEnum)