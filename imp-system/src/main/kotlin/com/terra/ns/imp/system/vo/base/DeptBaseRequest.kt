package com.terra.ns.imp.system.vo.base

import com.terra.ns.imp.system.constant.SystemConstants
import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Size

/**
@author qins
@date 2023/6/6
@desc 部门信息实体
 */
@Schema(description = "部门基础信息")
open class DeptBaseRequest (

    @Schema(description = "父部门编码, 顶级部门编码为${SystemConstants.TOP_DEPT_PARENT_CODE}", required = true)
    @get:NotBlank(message = "父级部门不能为空")
    var parentCode : String = SystemConstants.TOP_DEPT_PARENT_CODE,

    @Schema(description = "部门名称", required = true)
    @get:NotBlank(message = "部门名称不能为空")
    @get:Size(max=100, message = "部门名称长度不能超过100个字符")
    var name: String = "",

    @Schema(description = "部门负责人编码")
    var principalCode: String? = null,

    )