package com.terra.ns.imp.system.exception

import com.terra.ns.imp.common.core.exception.ErrorCode
import com.terra.ns.imp.common.core.exception.ServiceException
import com.terra.ns.imp.system.constant.SystemModule

/**
@author qins
@date 2023/6/1
@desc 系统用户模块异常
 */
class SystemServiceException(errorCode: ErrorCode, vararg params: String) : ServiceException(SystemModule.instance, errorCode, *params)