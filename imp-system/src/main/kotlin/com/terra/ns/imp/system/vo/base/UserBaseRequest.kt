package com.terra.ns.imp.system.vo.base

import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotEmpty
import jakarta.validation.constraints.Size

/**
@author qins
@date 2023/6/14
@desc
 */
@Schema(description = "用户基础信息")
open class UserBaseRequest {

    @Schema(description = "用户名称", required = true)
    @get:NotBlank(message = "用户名称不能为空")
    @get:Size(max=100, message = "用户名称长度不能超过100个字符")
    var name: String = ""

    @Schema(description = "邮箱")
    @get:Size(max=100, message = "用户邮箱长度不能超过100个字符")
    var email: String? = null

    @Schema(description = "头像地址")
    @get:Size(max=200, message = "用户头像地址长度不能超过200个字符")
    var avatar: String? = null

    @Schema(description = "入职时间 yyyy-mm-dd")
    @get:Size(max=10, message = "入职时间格式错误")
    var hiredDate: String? = null

    @Schema(description = "职位")
    @get:Size(max=100, message = "用户职位长度不能超过100个字符")
    var position: String? = null

    @Schema(description = "部门编码列表")
    @get:NotEmpty(message = "未指定部门")
    var deptCodeList: List<String> = emptyList()

    @Schema(description = "主部门编码")
    var mainDeptCode: String? = null
}