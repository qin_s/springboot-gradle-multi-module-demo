package com.terra.ns.imp.system.api

import com.terra.ns.imp.system.api.entity.StOperateLog
import com.terra.ns.imp.system.api.service.IOperateLogApi
import com.terra.ns.imp.system.mapper.StOperateLogMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
@author qins
@date 2023/6/5
@desc 操作日志API实现
 */
@Service
class OperateLogApiImpl: IOperateLogApi {

    @Autowired
    private lateinit var operateLogMapper: StOperateLogMapper

    override fun saveOperateLog(log: StOperateLog): Boolean {
        return operateLogMapper.insert(log) > 0
    }
}