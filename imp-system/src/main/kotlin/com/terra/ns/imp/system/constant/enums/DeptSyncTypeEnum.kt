package com.terra.ns.imp.system.constant.enums

/**
@author qins
@date 2023/6/7
@desc 企业部门同步类型
 */
enum class DeptSyncTypeEnum(val code: Int, val desc: String) {
    FULL(0, "全量同步"),
    INCREMENT(1, "增量同步"),
    ;

    companion object {
        fun getDesc(code: Int?): String? {
            code?.let {
                return values().firstOrNull { it.code == code }?.desc
            }
            return null
        }

        fun getEnum(code: Int?) : DeptSyncTypeEnum? {
            code?.let {
                return values().firstOrNull { it.code == code }
            }
            return null
        }
    }
}