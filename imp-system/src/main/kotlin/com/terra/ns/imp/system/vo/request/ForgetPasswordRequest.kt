package com.terra.ns.imp.system.vo.request

import com.terra.ns.imp.common.sms.vo.VerifyCaptchaEnableNullRequest
import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.constraints.NotBlank

/**
@author qins
@date 2023/6/14
@desc 忘记登录密码
 */
@Schema(description = "忘记登录密码-修改密码请求实体")
data class ForgetPasswordRequest (

    @Schema(description = "手机号", required = true)
    @get:NotBlank(message = "手机号不能为空")
    var phone: String = "",

    @Schema(description = "新密码", required = true)
    @get:NotBlank(message = "新密码不能为空")
    var newPassword: String = "",

    ) : VerifyCaptchaEnableNullRequest()