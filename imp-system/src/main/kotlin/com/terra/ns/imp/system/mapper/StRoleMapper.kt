package com.terra.ns.imp.system.mapper

import com.baomidou.mybatisplus.extension.kotlin.KtUpdateWrapper
import com.terra.ns.imp.common.mybatis.mapper.BaseMapperX
import com.terra.ns.imp.common.mybatis.mapper.KtQueryWrapperX
import com.terra.ns.imp.system.entity.StRole

/**
 * <p>
 * 角色信息表 Mapper 接口
 * </p>
 *
 * @author qins
 * @since 2023-06-05
 */
interface StRoleMapper : BaseMapperX<StRole> {

    fun selectListByCodes(codes: List<String>): List<StRole> {
        if(codes.isEmpty()) return emptyList()
        return selectList(KtQueryWrapperX(StRole::class.java).inX(StRole::code, codes))
    }

    fun selectOneByName(name: String): StRole?
        = selectOne(
            KtQueryWrapperX(StRole::class.java).likeIfPresent(StRole::name, name)
                .lastLimit1()
        )

    fun selectOneByCode(code: String): StRole?
        = selectOne(
            KtQueryWrapperX(StRole::class.java).eq(StRole::code, code)
        )

    fun deleteByCode(code: String)
        = delete(KtQueryWrapperX(StRole::class.java).eqX(StRole::code, code))

    fun selectLikeName(keywords: String?): List<StRole>
        = selectList(KtQueryWrapperX(StRole::class.java).likeIfPresent(StRole::name, keywords))

    fun updateRoleAllFieldByCode(updateRole: StRole, code: String, ignoreColumn: List<String>)
        = updateFieldIncludeNull(updateRole, KtUpdateWrapper(StRole::class.java).eq(StRole::code, code), ignoreColumn)
}
