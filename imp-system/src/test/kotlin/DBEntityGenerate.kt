
import com.baomidou.mybatisplus.generator.AutoGenerator
import com.baomidou.mybatisplus.generator.config.DataSourceConfig
import com.baomidou.mybatisplus.generator.config.GlobalConfig
import com.baomidou.mybatisplus.generator.config.StrategyConfig
import com.terra.ns.imp.system.LpSystemApplication
import org.junit.Test
import org.springframework.boot.test.context.SpringBootTest

/**
@author qins
@date 2023/06/05
@desc mybatisplus 代码生成
 */
@SpringBootTest(classes = [LpSystemApplication::class])
class DBEntityGenerate {


    @Test
    fun dbAutoGenerator () {
        val dataSourceConfig = DataSourceConfig.Builder(
            "jdbc:mysql://10.1.10.60:3306/lp_application?useUnicode=true&useSSL=false&characterEncoding=utf8&serverTimezone=Asia/Shanghai",
            "root",
            "123456"
        ).build()
//        dataSourceConfig.driverName = "com.mysql.cj.jdbc.Driver"
        val mpg = AutoGenerator(dataSourceConfig)

        val dbconfig = GlobalConfig.Builder()
            .outputDir("D://tmp//")
            .author("qins")
            .enableKotlin().build()

        val strategy = StrategyConfig.Builder().build()
        mpg.global(dbconfig).strategy(strategy).execute()

    }

}