dependencies {
    implementation(project(":imp-common:imp-common-web"))
    implementation(project(":imp-common:imp-common-log"))
    implementation(project(":imp-common:imp-common-mybatis"))
    implementation(project(":imp-common:imp-common-sms"))
    implementation(project(":imp-common:imp-common-dingtalk-contact"))
    implementation(project(":imp-common:imp-common-dingtalk"))
    implementation(project(":imp-common:imp-common-oss"))
    implementation(project(":imp-api:imp-system-api"))
    implementation(projectLibs.bundles.datasource)
    implementation(projectLibs.hutool.crypto)
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation(projectLibs.bundles.mybatisplus.generator)
}